DROP DATABASE IF EXISTS Trolling;
CREATE DATABASE IF NOT EXISTS Trolling;

DROP USER IF EXISTS 'trolling'@'localhost';
CREATE USER 'trolling'@'localhost' IDENTIFIED BY 'trolling';

GRANT ALL PRIVILEGES on Trolling.* to 'trolling'@'localhost';
FLUSH PRIVILEGES;

USE Trolling;

create table roles (
  id int primary key auto_increment,
  name varchar(32)			not null
);

create table users (
  id int primary key auto_increment,
  firstName varchar(32) 	not null,
  lastName varchar(32) 		not null,
  email varchar(64)			not null,
  address varchar(128) 		not null,
  password varchar(200) 		not null,
  telNumber varchar(12),
  role_id int				not null,
  foreign key (role_id) references roles(id)
);

create table states (
  id int primary key auto_increment,
  stateName varchar(32)			not null
);

create table kickscooters (
  id 								int primary key auto_increment,
  macAddress varchar(32)  			not null,
  kickScooterName varchar(32)		not null,
  state_id int 						default 1 not null,
  brand varchar(32)   default "undefined",
  type varchar(32)    default "undefined",
  number        int default -1,
  foreign key(state_id) references states(id)
);

create table kickscooter_data (
  id int primary key auto_increment,
  kickscooterId int 				not null,
  time_stamp datetime				not null,
  
  batteryCapacity float 		not null,
  batteryCurrent float         not null,
  batteryPercent float         not null,
  batteryTemperature float     not null,
  batteryTemperature2 float    not null,
  batteryVoltage float         not null,
  cellVoltagesId int  				not null,
 
  distanceLeftKm float         not null,
  odometerKm float             not null,
  speedKmh float               not null,
  tripDistanceM float          not null,
  
  isCruiseOn bool                   not null,
  isLockOn bool 					not null,
  isTailLightOn bool                not null,
  
  pin int                           not null,
  serialNr varchar(32)              not null,
  uptimeS varchar(32)		                not null,
  
  foreign key(kickscooterId) references kickscooters(id)
);

create table cell_voltages (
	cellVoltagesId int primary key  auto_increment,
    kickscooterId  int 				not null,
    
    cellVoltage_1  float        not null,
    cellVoltage_2  float        not null,
	cellVoltage_3  float        not null,
	cellVoltage_4  float        not null,
    cellVoltage_5  float        not null,
    cellVoltage_6  float        not null,
    cellVoltage_7  float        not null,
    cellVoltage_8  float        not null,
    cellVoltage_9  float        not null,
    cellVoltage_10 float        not null,
    
	foreign key(kickscooterId) references kickscooters(id)
);

create table rents (
  id int primary key auto_increment,
  userId int 			not null,
  kickscooterId int 	not null,
  rentDate datetime 		not null,
  returnedDate datetime,
  kmPassed float default 0,
  foreign key(userId) references users(id),
  foreign key(kickscooterId) references kickscooters(id)
);

INSERT INTO states (stateName) 
VALUES 
	("not published"),
	("available"),
    ("under maintenance"),
    ("rented"),
    ("not found")
;
insert into roles (name) values ('user');
insert into roles (name) values ('admin');

insert into users(firstName,lastName,email,address,password,telNumber,role_id) 
values ("Admin","Admin","admin@gmail.com","Cluj","d1cde049046763854eb8bd838479b33a5b5df8316ddfcbac2cd7779ee644aceb","0723456789",2);
