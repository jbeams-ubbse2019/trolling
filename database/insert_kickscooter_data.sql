USE trolling_seq;

DROP PROCEDURE IF EXISTS insert_kickscooter;
CREATE PROCEDURE insert_kickscooter (pMacAddress varchar(32), pKickScooterName varchar(32), pPin int, pSerialNr varchar(32) )
MODIFIES SQL DATA
BEGIN
	INSERT INTO kickscooters (macAddress, name, pin, serialNr) VALUES (pMacAddress, pKickScooterName, pPin, pSerialNr);
END;

DROP PROCEDURE IF EXISTS insert_kickscooter_data;
CREATE PROCEDURE insert_kickscooter_data (
  pMacAddress varchar(32),
  pKickScooterName varchar(32),

  pBatteryCapacity float,
  pBatteryCurrent float,
  pBatteryPercent float,
  pBatteryTemperature float,
  pBatteryTemperature2 float,
  pBatteryVoltage float,
  
  pCellVoltage_1   float,
  pCellVoltage_2   float,
  pCellVoltage_3   float,
  pCellVoltage_4   float,
  pCellVoltage_5   float,
  pCellVoltage_6   float,
  pCellVoltage_7   float,
  pCellVoltage_8   float,
  pCellVoltage_9   float,
  pCellVoltage_10  float,  
  
  pDistanceLeftKm float,
  pOdometerKm float,
  pSpeedKmh float,
  pTripDistanceM float,
  
  pIsCruiseOn bool,
  pIsLockOn bool,
  pIsTailLightOn bool,
  
  pPin int,
  pSerialNr varchar(32),
  pUptimeS varchar(255)
)
MODIFIES SQL DATA
BEGIN
    DECLARE thisKickscooterId int;
    DECLARE theseCellVoltagesId int;
    
    SET thisKickscooterId = -1;
    SET thisKickscooterId = (SELECT id FROM kickscooters WHERE macAddress = pMacAddress);
    
    IF (ISNULL(thisKickscooterId)) THEN
	    CALL insert_kickscooter(pMacAddress, pKickScooterName, pPin, pSerialNr);
      SET thisKickscooterId = last_insert_id();
	  END IF;

    IF ((SELECT pin FROM kickscooters WHERE macAddress = pMacAddress) = 0 AND 
    (SELECT serialNr FROM kickscooters WHERE macAddress = pMacAddress) = "") THEN
      UPDATE kickscooters
      SET serialNr = pSerialNr, pin = pPin
      WHERE id = thisKickscooterId;
    END IF;
    
    INSERT INTO cellVoltages (
		cellVoltage1,
		cellVoltage2,
		cellVoltage3,
		cellVoltage4,
		cellVoltage5,
		cellVoltage6,
		cellVoltage7,
		cellVoltage8,
		cellVoltage9,
		cellVoltage10		
    ) 
    VALUES (
        pCellVoltage_1,
        pCellVoltage_2,
		    pCellVoltage_3,
        pCellVoltage_4,
        pCellVoltage_5,
        pCellVoltage_6,
        pCellVoltage_7,
        pCellVoltage_8,
		    pCellVoltage_9,
        pCellVoltage_10
    );
    
	SET theseCellVoltagesId = LAST_INSERT_ID();
    
	INSERT INTO kickscooterData (
	  kickscooterId,
	  timeStamp,
	  
	  batteryCapacity,
	  batteryCurrent,
	  batteryPercent,
	  batteryTemperature,
	  batteryTemperature2,
	  batteryVoltage,
    cellVoltageId,
	 
	  distanceLeftKm,
	  odometerKm,
	  speedKmh,
	  tripDistanceM,
	  
	  isCruiseOn,
	  isLockOn,
	  isTailLightOn,
	  
	  uptimeS
	)
	VALUES (
		thisKickscooterId,
		NOW(),

		pBatteryCapacity,
		pBatteryCurrent,
		pBatteryPercent,
		pBatteryTemperature,
		pBatteryTemperature2,
		pBatteryVoltage,
		theseCellVoltagesId,

		pDistanceLeftKm,
		pOdometerKm,
		pSpeedKmh,
		pTripDistanceM,

		pIsCruiseOn,
		pIsLockOn,
		pIsTailLightOn,

		pUptimeS
	);
        
END;

DROP PROCEDURE IF EXISTS insert_foreign_kickscooter;
CREATE PROCEDURE insert_foreign_kickscooter (
  pMacAddress varchar(32),
  pKickScooterName varchar(32),
  pPin int,
  pSerialNr varchar(32)
)
MODIFIES SQL DATA
BEGIN
    DECLARE thisKickscooterId int;
    
    SET thisKickscooterId = -1;
    SET thisKickscooterId = (SELECT id FROM kickscooters WHERE macAddress = pMacAddress);
    
    IF (ISNULL(thisKickscooterId)) THEN
	CALL insert_kickscooter(pMacAddress, pKickScooterName, pPin, pSerialNr);
    END IF;
    
        
END;
