"""Script to insert collected data into the database"""
import json
import sys

from configparser import ConfigParser

import mysql.connector

PARSER = ConfigParser()
PARSER.read('/home/pi/trolling/database/config.ini')

MY_HOST = PARSER.get("db", "host")
MY_USER_NAME = PARSER.get("db", "user")
MY_PASSWD = PARSER.get("db", "passwd")
MY_DATABASE_NAME = PARSER.get("db", "database")

MY_DB = mysql.connector.connect(
    host=MY_HOST,
    user=MY_USER_NAME,
    passwd=MY_PASSWD,
    database=MY_DATABASE_NAME
)

INPUT_FILE = open("/home/pi/trolling/flask/json_data.txt", "r")
DATA_FROM_JSON = json.load(INPUT_FILE)
MY_CURSOR = MY_DB.cursor()

MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

try:

    DATA = (
        MAC_ADDRESS,
        SCOOTER_NAME,
        DATA_FROM_JSON['remaining_capacity'],
        DATA_FROM_JSON['battery_current'],
        DATA_FROM_JSON['battery_percent'],
        DATA_FROM_JSON['battery_temperature_1'],
        DATA_FROM_JSON['battery_temperature_2'],
        DATA_FROM_JSON['battery_voltage'],
        DATA_FROM_JSON['cell_voltages'][0],
        DATA_FROM_JSON['cell_voltages'][1],
        DATA_FROM_JSON['cell_voltages'][2],
        DATA_FROM_JSON['cell_voltages'][3],
        DATA_FROM_JSON['cell_voltages'][4],
        DATA_FROM_JSON['cell_voltages'][5],
        DATA_FROM_JSON['cell_voltages'][6],
        DATA_FROM_JSON['cell_voltages'][7],
        DATA_FROM_JSON['cell_voltages'][8],
        DATA_FROM_JSON['cell_voltages'][9],
        DATA_FROM_JSON['km_left'],
        DATA_FROM_JSON['total_km'],
        DATA_FROM_JSON['speed'],
        DATA_FROM_JSON['trip_distance'],
        DATA_FROM_JSON['is_cruise_on'],
        DATA_FROM_JSON['is_lock_on'],
        DATA_FROM_JSON['is_tail_light_on'],
        DATA_FROM_JSON['pin'],
        DATA_FROM_JSON['vehicle_serial_number'],
        DATA_FROM_JSON['uptime'],
    )
except Exception as my_exception:
    print("Cant find a key")
    print(my_exception)
    sys.exit(1)

print(MY_CURSOR.callproc("insert_kickscooter_data", DATA))
MY_CURSOR.execute('COMMIT')
MY_DB.disconnect()
sys.exit(0)
