create or replace package users is

function add_user(p_firstName varchar, p_lastName varchar, email varchar,
				  p_adress varchar, p_password varchar, p_telNumber varchar,
                  p_usingLickscooter varchar default 'N', role_id int) return number;

end users;