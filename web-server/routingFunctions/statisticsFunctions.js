const checkFunc = require('../util/inputCheckFunctions');

exports.correctDateInterval = (d1, d2) => new Promise((resolve, reject) => {
  try {
    checkFunc.checkDateInterval(d1, d2);
    resolve('ok');
  } catch (e) {
    reject(new Error(`Frame temperature figure error: ${e.message}`));
  }
});
