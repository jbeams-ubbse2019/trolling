const checkFunc = require('../util/inputCheckFunctions'),
  log = require('../config/myLogger'),
  dbScooter = require('../db/dbKickscooters');

exports.checkData = (scooterID) => new Promise((resolve, reject) => {
  try {
    checkFunc.checkIfNumber(scooterID);
    checkFunc.checkIfScooterExistsByID(scooterID)
      .then(() => resolve('ok'))
      .catch((err) => {
        reject(new Error(err.message));
      });
  } catch (e) {
    log.debug(`kickscootersFunctions checkData: ${e.message}`);
    reject(new Error(e.message));
  }
});

exports.getData = (scooterID) => new Promise((resolve, reject) => {
  dbScooter.getKickscooterToShowById(scooterID)
    .then((resp) => {
      resolve(resp);
    })
    .catch((err) => {
      log.err(`kickscootersFunctions getData: ${err.message}`);
      reject(new Error('Database error :( '));
    });
});

exports.getKickscooterDataById = (ID) => new Promise((resolve, reject) => {
  dbScooter.getDataByID(ID)
    .then((response) => resolve(response))
    .catch((err) => {
      log.error(`kickscootersFunctions getMacAddressById: ${err.message}`);
      reject(new Error('Database error'));
    });
});

exports.updateStatenameByID = (scooterID, stateName) => new Promise((resolve, reject) => {
  dbScooter.updateStateName(scooterID, stateName)
    .then(() => resolve('State updated succesfully'))
    .catch((err) => {
      log.error(`kickscootersFunctions updateStatenameById: ${err.message}`);
      reject(new Error('Sorry, internal server error :(, please try again later'));
    });
});

exports.updateBasicDataByID = (scooterID, number, brand, type) => new Promise((resolve, reject) => {
  dbScooter.updateBasicKsData(scooterID, number, brand, type)
    .then(() => resolve('Data was succesfully updated'))
    .catch((err) => {
      log.error(`kickscootersFunctions updateBasicDataByID: ${err.message}`);
      reject(new Error('Sorry, internal server error :(, please try again later'));
    });
});
