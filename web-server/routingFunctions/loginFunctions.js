const dbUsers = require('../db/dbUsers'),
  hash = require('../util/hashPasswd'),
  checkFunc = require('../util/inputCheckFunctions'),
  log = require('../config/myLogger');


exports.checkLoginData = (email, passwd) => new Promise((resolve, reject) => {
  try {
    checkFunc.checkEmail(email);
    checkFunc.checkPasswd(passwd);
    resolve('ok');
  } catch (e) {
    reject(new Error(`Error, check login data: ${e.message}`));
  }
});


function loginResolvePassword(email, passwd) {
  return new Promise((resolve, reject) => {
    dbUsers.getPassword(email)
      .then((hashedPasswd) => {
        if (!hashedPasswd) {
          log.debug('checkIfUserExists: There is no such user');
          reject(new Error('Error: Invalid login data'));
        } else {
          hash.deHashPasswd(passwd, hashedPasswd)
            .then(() => {
              dbUsers.getUserId(email)
                .then((result) => {
                  resolve(result.toString());
                })
                .catch((err) => {
                  log.error(err.message);
                  reject(new Error('Database error :( '));
                });
            })
            .catch((e) => reject(new Error(e.message)));
        }
      })
      .catch((err) => {
        log.error(`loginFunctions loginResolvePassword: ${err.message}`);
        reject(new Error('Database error'));
      });
  });
}

exports.checkIfUserExists = (email, passwd) => new Promise((resolve, reject) => {
  dbUsers.checkIfUserExistsWithMail(email)
    .then((nrOfUsers) => {
      if (!nrOfUsers) {
        log.debug('checkIfUserExists: There is no such user');
        reject(new Error('Error: invalid login data'));
      } else {
        loginResolvePassword(email, passwd)
          .then((resp) => resolve(resp))
          .catch((error) => reject(new Error(error.message)));
      }
    })
    .catch((err) => {
      log.error(`loginFunctions checkIfUserExistst: ${err.message}`);
    });
});
