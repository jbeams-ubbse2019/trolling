const dbRents = require('../db/dbRents'),
  log = require('../config/myLogger');

exports.rentAKickscooter = (userId, kickscooterId, statusName) => new Promise((resolve, reject) => {
  dbRents.changeRentStatus(kickscooterId, statusName)
    .then(() => dbRents.rentAKickscooter(userId, kickscooterId))
    .then(() => resolve('Rented'))
    .catch((err) => {
      log.error(`rentingFunctions rentAKickscooter: ${err.message}`);
      reject(new Error('Database error'));
    });
});


exports.bringBackAKickscooter = (kickscooterId, rentId, statusName) => new Promise((resolve, reject) => {
  dbRents.changeRentStatus(kickscooterId, statusName)
    .then(() => dbRents.bringBackAKickscooter(kickscooterId, rentId))
    .then(() => resolve('Bringed back'))
    .catch((err) => {
      log.error(`rentingFunctions bringBackAKickscooter: ${err.message}`);
      reject(new Error('Database error'));
    });
});
