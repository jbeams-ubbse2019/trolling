const checkFunctions = require('../util/inputCheckFunctions'),
  dbUsers = require('../db/dbUsers'),
  log = require('../config/myLogger');

function getUserRole(email, resolve) {
  dbUsers.checkIfUserExistsWithMail(email)
    .then((exists) => {
      if (exists) {
        dbUsers.getUserRoleByMail(email)
          .then((role) => resolve(role))
          .catch((err) => {
            log.error(err.message);
            throw new Error('Database error');
          });
      } else {
        log.error('getUserRole function: The user doesn\'t exists');
        throw new Error('Invalid data');
      }
    })
    .catch((err) => {
      log.error(err.message);
      throw new Error('Database error');
    });
}

exports.checkData = (email) => new Promise((resolve, reject) => {
  try {
    checkFunctions.checkEmail(email);
    getUserRole(email, resolve);
  } catch (e) {
    reject(e.message);
  }
});
