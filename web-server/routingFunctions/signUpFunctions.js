const checkFunc = require('../util/inputCheckFunctions'),
  dbUsers = require('../db/dbUsers'),
  log = require('../config/myLogger');

exports.checkSignUpData = (userData) => new Promise((resolve, reject) => {
  try {
    checkFunc.checkFirstName(userData.firstName);
    checkFunc.checkLastName(userData.lastName);
    checkFunc.checkEmail(userData.email);
    checkFunc.checkAddress(userData.address);
    checkFunc.checkPasswdSignUp(userData.password, userData.confirmPwd);
    checkFunc.checkTelNumber(userData.telNumber);
    resolve('ok');
  } catch (e) {
    reject(new Error(`Error, check sign up data: ${e.message}`));
  }
});

exports.checkIfUserRegistered = (email) => new Promise((resolve, reject) => {
  dbUsers.checkIfUserExistsWithMail(email)
    .then((nrOfUsers) => {
      if (nrOfUsers) {
        log.error('checkIfUserRegistered: User already exists');
        reject(new Error('User already exists'));
      } else {
        resolve('ok');
      }
    })
    .catch((err) => {
      log.error(err);
      reject(new Error('Database error'));
    });
});

exports.insertANewUser = (data) => new Promise((resolve, reject) => {
  dbUsers.insertNewUser(data)
    .then(() => resolve('New user has been inserted'))
    .catch((err) => {
      log.error(`signUpFunctions insertANewUser: ${err.message}`);
      reject(new Error('Database error :( '));
    });
});
