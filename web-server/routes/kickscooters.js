const express = require('express'),
  dbKickscooters = require('../db/dbKickscooters'),
  log = require('../config/myLogger'),
  kickFunc = require('../routingFunctions/kickscootersFunctions'),
  statFunc = require('../routingFunctions/statisticsFunctions'),
  checkFunc = require('../util/inputCheckFunctions');

const app = express.Router();

app.get('/toShow/:ID', (req, resp) => {
  const {
    ID,
  } = req.params;

  log.debug(`/kickscooters/toShow/:scooterID get request: ${ID}`);

  kickFunc.checkData(ID)
    .then(() => kickFunc.getData(ID))
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`/kickscooters/toShow/scooterID get request error ${err.message} `);
      resp.status(400).send(err.message);
    });
});

app.get('/filters/mainList', (req, resp) => {
  log.debug('get request /filters/mainList');

  dbKickscooters.getScootersMainList()
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`kickscooters/filters/mainList get request: ${err.message}`);
      resp.status(500).send('Sorry, internal server error :(, please try again later');
    });
});

app.get('/filters/states/:stateName', (req, resp) => {
  const { stateName } = req.params;
  log.debug(`get request /filters/states/${stateName}`);

  checkFunc.checkIfStateExistsByName(stateName)
  .then(() =>  dbKickscooters.getScootersMainListByState(stateName))
  .then((data) => resp.send(data))
  .catch((err) => {
    log.error(`kickscooters/filters/states/${stateName} get request: ${err.message}`);
    resp.status(500).send(err);
  });
});

app.get('/states', (req, resp) => {
  log.debug('/kickscooters/states get request');

  dbKickscooters.getAllStateNames()
    .then((result) => resp.send(result))
    .catch((err) => {
      log.error(`/kicksccoters/states get request database error ${err.message}`);
      resp.status(500).send('Sorry, internal server error :(, please try again later');
    });
});

app.put('/states', (req, resp) => {
  const { scooterID, stateName } = req.query;

  log.debug(`PUT /kickscooters/states with scooterID = ${scooterID}, stateName = ${stateName}`);

  checkFunc.checkIfScooterExistsByID(scooterID)
    .then(() => checkFunc.checkIfStateExistsByName(stateName))
    .then(() => kickFunc.updateStatenameByID(scooterID, stateName))
    .then((message) => resp.send(message))
    .catch((err) => resp.status(500).send(err));
});

app.get('/filters/available', (req, resp) => {
  log.debug('get request /filters/available');

  dbKickscooters.getAvailableKickScooters()
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`kickscooters/filters/available get request: ${err.message}`);
      resp.send(err.message);
    });
});

app.get('/filters/lastCellVoltages', (req, resp) => {
  const {
    scooterID,
  } = req.query;

  log.debug(`get request /filters/lastCellVoltages with scooterID = ${scooterID}`);

  kickFunc.checkData(scooterID)
    .then(() => dbKickscooters.getLastCellVoltages(scooterID)
      .then((data) => resp.send(data))
      .catch((err) => {
        log.error(`kickscooters/filters/lastCellVoltages with scooterID = ${scooterID} get request: ${err.message}`);
        resp.send(err.message);
      }));
});

app.get('/filters/batteryTemperatureWithDateInterval', (req, resp) => {
  const {
    scooterID,
    startDate,
    endDate,
  } = req.query;

  log.debug(`get request /filters/frameTemperatureWithDateInterval with scooterID = ${scooterID} startDate = ${startDate} endDate = ${endDate}`);

  kickFunc.checkData(scooterID)
    .then(() => statFunc.correctDateInterval(startDate, endDate)
    .then(() => dbKickscooters.batteryTemperatureWithDateInterval(scooterID, startDate, endDate))
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`kickscooters/filters/frameTemperatureWithDateInterval with scooterID = ${scooterID} startDate = ${startDate} endDate = ${endDate} get request: ${err.message}`);
      resp.status(400).send(err.message);
    })
)});

app.get('/filters/totalKmPassed', (req, resp) => {
  log.debug('get request /filters/totalKmPassed/');

  dbKickscooters.getTotalScooterKm()
    .then((data) => {
      if (data[0].kmSum) {
        const temp = data[0].kmSum.toFixed(2);
        data[0].kmSum = temp;
      } else {
        data[0].kmSum = 0;
      }
      return data;
    })
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`kickscooters/filters/totalKmPassed get request: ${err.message}`);
      resp.send(err.message);
    });
});

app.get('/:ID', (req, resp) => {
  const { ID } = req.params;

  log.debug(`/kickscooters/:ID get request: ${ID}`);

  checkFunc.checkIfScooterExistsByID(ID)
    .then(() => kickFunc.getKickscooterDataById(ID))
    .then((result) => resp.send(result))
    .catch((err) => {
      log.error(err.message);
      resp.status(500).send(err.message);
    });
});

app.patch('/', (req, resp) => {
  const { scooterID, number, brand, type } = req.query;

  log.debug(`PATCH /kickscooters with scooterID = ${scooterID}, number = ${number}, brand = ${brand}, type = ${type}`);

  checkFunc.checkIfScooterExistsByID(scooterID)
    .then(() => kickFunc.updateBasicDataByID(scooterID, number, brand, type))
    .then((message) => resp.send(message))
    .catch((err) => resp.status(500).send(err));
});

module.exports = app;
