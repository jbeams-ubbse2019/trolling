const express = require('express');

const log = require('../config/myLogger'),
  dbUser = require('../db/dbUsers'),
  dbRents = require('../db/dbRents'),
  checkFunc = require('../util/inputCheckFunctions'),
  rentFunc = require('../routingFunctions/rentingFunctions');

const app = express.Router();

// current rents
app.get('/user/:userID', (req, resp) => {
  const {
    userID,
  } = req.params;

  log.debug(`/rents/user/:userId GET request with userID = ${userID}`);
  dbUser.checkIfUserExistsWithID(userID)
    .then((result) => {
      if (result) {
        dbRents.getAllForAUser(userID)
          .then((response) => {
            resp.send(response);
          })
          .catch((error) => {
            log.error(error);
            resp.status(500).send('Database error');
          });
      } else {
        resp.status(500).send('No renting data');
      }
    })
    .catch((err) => {
      log.error(err.message);
      resp.status(500).send('Database error');
    });
});

app.post('/', (req, resp) => {
  const {
    userId,
    kickscooterId,
    statusName
  } = req.body;

  log.debug(`POST request to /rents with params userId = ${userId}, kickscooterId = ${kickscooterId}, statusName = ${statusName}`);

  checkFunc.checkIfUserExistsByUserID(userId)
    .then(() => checkFunc.checkIfUserHaveNotRentedYet(userId))
    .then(() => checkFunc.checkIfScooterExistsByID(kickscooterId))
    .then(() => rentFunc.rentAKickscooter(userId, kickscooterId, statusName))
    .then((response) => resp.send(response))
    .catch((err) => resp.status(500).send(err.message));
});

app.post(('/bringBack'), (req, resp) => {
  const {
    kickscooterId,
    rentId,
    statusName
  } = req.body;

  log.debug(`POST request to /rent/bringBack with params rentID ${rentId}, kickscooterId ${kickscooterId}, statusName = ${statusName}`);

  checkFunc.checkIfScooterExistsByID(kickscooterId)
    .then(() => rentFunc.bringBackAKickscooter(kickscooterId, rentId, statusName))
    .then((message) => resp.send(message))
    .catch((err) => resp.status(500).send(err.message));
});

app.get('/filters/totalRentFrequency', (req, resp) => {
  log.debug('get request /filters/totalRentFrequency');

  dbRents.rentsByMonth()
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`rents/filters/totalRentFrequency get request: ${err.message}`);
      resp.send(err.message);
    });
});

app.get('/', (req, resp) => {
  const {
    userID
  } = req.query;

  log.debug(`get request to /rents with parameters userID = ${userID}`);

  dbRents.allRentData(userID)
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`/rents get request: ${err.message}`);
      resp.send(err.message);
    });
});

app.get('/filters/rentFrequencyPerVehicle', (req, resp) => {
  log.debug('get request /filters/rentFrequencyPerVehicle');

  dbRents.rentsByVehicle()
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`rents/filters/rentFrequencyPerVehicle get request: ${err.message}`);
      resp.send(err.message);
    });
});

app.get('/all/:userID', (req, resp) => {
  const { userID } = req.params;

  log.debug(`get request to /rents/${userID}`);

  dbRents.allPastRentData(userID)
    .then((data) => resp.send(data))
    .catch((err) => {
      log.error(`/rents/${userID} get request: ${err.message}`);
      resp.send(err.message);
    });
});

module.exports = app;
