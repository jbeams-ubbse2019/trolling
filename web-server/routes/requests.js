const express = require('express'),
  loginFunc = require('../routingFunctions/loginFunctions'),
  signUpFunc = require('../routingFunctions/signUpFunctions'),
  hash = require('../util/hashPasswd'),
  log = require('../config/myLogger');

const router = express.Router();

router.post('/login', (req, resp) => {
  const {
    email,
    password,
  } = req.body;

  log.debug(`POST request to login with params email ${email}`);

  loginFunc.checkLoginData(email, password)
    .then(() => loginFunc.checkIfUserExists(email, password))
    .then((message) => resp.status(200).send(message))
    .catch((err) => {
      log.error(`Error: /requests/login post error: ${err.message}`);
      resp.status(400).send(err.message);
    });
});

router.post('/signUp', (req, resp) => {
  const {
    firstName,
    lastName,
    email,
    address,
    password,
    confirmPassword,
    telNumber,
  } = req.body;

  const data = {};
  data.firstName = firstName;
  data.lastName = lastName;
  data.email = email;
  data.address = address;
  data.password = password;
  data.confirmPwd = confirmPassword;
  data.telNumber = telNumber;

  log.debug(`POST request to sign up with params firstName: ${data.firstName}, lastName: ${data.lastName}, email: ${data.email}, address: ${data.address}, telNumber: ${data.telNumber}`);

  signUpFunc.checkSignUpData(data)
    .then(() => signUpFunc.checkIfUserRegistered(data.email))
    .then(() => hash.hashPasswd(data.password))
    .then((hashedPwd) => {
      data.password = hashedPwd;
      signUpFunc.insertANewUser(data);
    })
    .then(() => resp.send())
    .catch((err) => {
      log.error(err.message);
      resp.status(500).send(err.message);
    });
});

module.exports = router;
