const express = require('express'),
  log = require('../config/myLogger'),
  dbUsers = require('../db/dbUsers'),
  checkFunc = require('../util/inputCheckFunctions'),
  getRoleFunc = require('../routingFunctions/usersGetRoleFunctions');

const app = express.Router();

app.get('/role', (req, resp) => {
  const { email } = req.query;

  log.debug(`Get role request with email: ${email}`);

  getRoleFunc.checkData(email)
    .then((userRole) => resp.send(userRole))
    .catch((e) => {
      log.debug(`/users/role get request error: ${e.message}`);
      resp.status().send(e.message);
    });
});

app.get('/names/:ID', (req, resp) => {
  const { ID } = req.params;

  log.debug(`Get name request with id: ${ID}`);

  checkFunc.checkIfUserExistsByUserID(ID)
  .then(() => dbUsers.getUserNameByID(ID))
  .then((data) => resp.send(data))
  .catch((err) => {
    log.error(`users/names/${ID} get request: ${err.message}`);
    resp.send(err.message); 
  });
});

module.exports = app;
