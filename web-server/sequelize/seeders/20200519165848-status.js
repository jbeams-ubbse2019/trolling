'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('status', [
      { name: 'not published' },
      { name: 'available' },
      { name: 'under maintenance' },
      { name: 'rented' },
      { name: 'not found' }, 
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('status', null, {});
  }
};
