'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
      firstName: 'Admin',
      lastName: 'Admin',
      email: 'admin@gmail.com',
      address: 'Cluj',
      password: 'd1cde049046763854eb8bd838479b33a5b5df8316ddfcbac2cd7779ee644aceb',
      telNumber: '0723456789',
      roleId: Sequelize.literal(`(
        SELECT id
        FROM roles
        WHERE name = "admin"
    )`)
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
