'use strict';
module.exports = (sequelize, DataTypes) => {
  const kickscooter = sequelize.define('kickscooter', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    macAddress: {
      type: DataTypes.STRING(32),
      allowNull: false,
      unique: true
    },
    name: {
      type: DataTypes.STRING(32),
      allowNull: false,
      unique: true
    },
    brand: {
      type: DataTypes.STRING(32),
      defaultValue: "undefined"
    },
    type: {
      type: DataTypes.STRING(32),
      defaultValue: "undefined"
    },
    number: {
      type: DataTypes.INTEGER,
      defaultValue: -1
    },
    pin: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    serialNr: {
      type: DataTypes.STRING(32),
      allowNull: false
    }
  }, {
    timestamps: false
  });
  kickscooter.associate = function(models) {
    kickscooter.belongsTo(models.status);
    kickscooter.hasMany(models.rent);
    kickscooter.hasMany(models.kickscooterData);
  };
  return kickscooter;
};