'use strict';
module.exports = (sequelize, DataTypes) => {
  const role = sequelize.define('role', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(32)
    }
  }, {
    timestamps: false
  });

  role.associate = function(models) {
    role.hasMany(models.user);
  };
  
  return role;
};