'use strict';
module.exports = (sequelize, DataTypes) => {
  const kickscooterData = sequelize.define('kickscooterData', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    timeStamp: {
      allowNull: false,
      type: DataTypes.DATE
    },

    batteryCapacity: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    batteryCurrent: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    batteryPercent: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    batteryTemperature: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    batteryTemperature2: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    batteryVoltage: {
      allowNull: false,
      type: DataTypes.FLOAT
    },

    distanceLeftKm: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    odometerKm: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    speedKmh: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    tripDistanceM: {
      allowNull: false,
      type: DataTypes.FLOAT
    },

    isCruiseOn: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    isLockOn: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    isTailLightOn: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },

    uptimeS: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    timestamps: false
  });
  kickscooterData.associate = function(models) {
    kickscooterData.belongsTo(models.cellVoltage);
    kickscooterData.belongsTo(models.kickscooter);
  };
  return kickscooterData;
};