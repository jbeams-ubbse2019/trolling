'use strict';
module.exports = (sequelize, DataTypes) => {
  const cellVoltage = sequelize.define('cellVoltage', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    cellVoltage1: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage2: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage3: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage4: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage5: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage6: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage7: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage8: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage9: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cellVoltage10: {
      type: DataTypes.FLOAT,
      allowNull: false
    }
  }, {
    timestamps: false
  });
  cellVoltage.associate = function(models) {
    // associations can be defined here
  };
  return cellVoltage;
};