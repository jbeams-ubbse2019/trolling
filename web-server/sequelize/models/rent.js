'use strict';
module.exports = (sequelize, DataTypes) => {
  const rent = sequelize.define('rent', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    rentDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    returnDate: DataTypes.DATE,
    kmTaken: {
      type: DataTypes.FLOAT,
      defaultValue: 0
    }
  }, {
    timestamps: false
  });
  rent.associate = function(models) {
    rent.belongsTo(models.user);
    rent.belongsTo(models.kickscooter);
  };
  return rent;
};