'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    firstName: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING(128),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    telNumber: {
      type: DataTypes.STRING(12),
      allowNull: false
    }
  }, {
    timestamps: false
  });

  user.associate = function(models) {
    user.belongsTo(models.role);
    user.hasMany(models.rent);
  };
  
  return user;
};