'use strict';
module.exports = (sequelize, DataTypes) => {
  const status = sequelize.define('status', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(32)
    }
  }, {
    timestamps: false,
    freezeTableName: true
  });

  status.associate = function(models) {
    status.hasMany(models.kickscooter);
  };
  
  return status;
};