'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // rent belongsTo user
    return queryInterface.addColumn(
      'rents', // name of Source table
      'userId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'users', // name of Target table
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'rents', // name of Source table
      'userId' // key we want to remove
    );
  }
};
