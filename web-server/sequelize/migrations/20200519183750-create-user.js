'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING(32),
        allowNull: false
      },
      lastName: {
        type: Sequelize.STRING(32),
        allowNull: false
      },
      email: {
        type: Sequelize.STRING(32),
        allowNull: false
      },
      address: {
        type: Sequelize.STRING(128),
        allowNull: false
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      telNumber: {
        type: Sequelize.STRING(12),
        allowNull: false
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};