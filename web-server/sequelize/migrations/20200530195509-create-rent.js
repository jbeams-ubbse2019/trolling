'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('rents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rentDate: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      returnDate: Sequelize.DATE,
      kmTaken: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('rents');
  }
};