'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // ksData belongsTo cellVoltage
    return queryInterface.addColumn(
      'kickscooterData', // name of Source table
      'cellVoltageId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'cellVoltages', // name of Target table
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'kickscooterData', // name of Source table
      'cellVoltageId' // key we want to remove
    );
  }
};