'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // ksData belongsTo kickscooter
    return queryInterface.addColumn(
      'kickscooterData', // name of Source table
      'kickscooterId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'kickscooters', // name of Target table
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'kickscooterData', // name of Source table
      'kickscooterId' // key we want to remove
    );
  }
};