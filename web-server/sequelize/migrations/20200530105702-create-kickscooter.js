'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('kickscooters', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      macAddress: {
        type: Sequelize.STRING(32),
        allowNull: false,
        unique: true
      },
      name: {
        type: Sequelize.STRING(32),
        allowNull: false,
        unique: true
      },
      brand: {
        type: Sequelize.STRING(32),
        defaultValue: "undefined"
      },
      type: {
        type: Sequelize.STRING(32),
        defaultValue: "undefined"
      },
      number: {
        type: Sequelize.INTEGER,
        defaultValue: -1
      },
      pin: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      serialNr: {
        type: Sequelize.STRING(32),
        allowNull: false
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('kickscooters');
  }
};