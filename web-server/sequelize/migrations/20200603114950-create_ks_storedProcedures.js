'use strict';
const fs = require('fs').promises,
      db = require('../models');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return fs.readFile('/home/pi/trolling/database/insert_kickscooter_data.sql')
      .then(sql => db.sequelize.query(sql.toString()));
  },

  down: (queryInterface, Sequelize) => {
    const dropProcedures = 'DROP PROCEDURE IF EXISTS insert_kickscooter; ' + 
      'DROP PROCEDURE IF EXISTS insert_kickscooter_data; ' + 
      'DROP PROCEDURE IF EXISTS insert_foreign_kickscooter;';
    return db.sequelize.query(dropProcedures);
  }
};
