'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('kickscooterData', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      timeStamp: {
        allowNull: false,
        type: Sequelize.DATE
      },

      batteryCapacity: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      batteryCurrent: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      batteryPercent: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      batteryTemperature: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      batteryTemperature2: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      batteryVoltage: {
        allowNull: false,
        type: Sequelize.FLOAT
      },

      distanceLeftKm: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      odometerKm: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      speedKmh: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      tripDistanceM: {
        allowNull: false,
        type: Sequelize.FLOAT
      },

      isCruiseOn: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      isLockOn: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      isTailLightOn: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },

      uptimeS: {
        allowNull: false,
        type: Sequelize.STRING
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('kickscooterData');
  }
};