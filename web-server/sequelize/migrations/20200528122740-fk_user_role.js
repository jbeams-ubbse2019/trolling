'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // user belongsTo role
    return queryInterface.addColumn(
      'users', // name of Source table
      'roleId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'roles', // name of Target table
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    // remove user belongsTo role
    return queryInterface.removeColumn(
      'users', // name of Source table
      'roleId' // key we want to remove
    );
  }
};
