'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // ks belongsTo status
    return queryInterface.addColumn(
      'kickscooters', // name of Source table
      'statusId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        defaultValue: 1,
        references: {
          model: 'status', // name of Target table
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'kickscooters', // name of Source table
      'statusId' // key we want to remove
    );
  }
};
