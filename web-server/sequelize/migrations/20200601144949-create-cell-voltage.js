'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('cellVoltages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cellVoltage1: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage2: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage3: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage4: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage5: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage6: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage7: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage8: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage9: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      cellVoltage10: {
        allowNull: false,
        type: Sequelize.FLOAT
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('cellVoltages');
  }
};