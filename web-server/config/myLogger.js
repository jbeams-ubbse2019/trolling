// https://github.com/winstonjs/logform#readme

const {
  createLogger,
  transports,
  format,
} = require('winston');

const logger = createLogger({
  format: format.combine(
    format.colorize(),
    format.timestamp(),
    format.align(),
    format.printf((info) => ` ${info.level} --- ${info.timestamp} : ${info.message}`),
  ),
  transports: [
    new transports.File({
      filename: './webserverErrorLogfile.log',
      level: 'error',
    }),
    new transports.Console({
      level: 'debug',
    }),
    new transports.Console({
      level: 'error',
    }),
  ],
});

module.exports = logger;
