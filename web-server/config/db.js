const mysql = require('mysql2'),
  db = require('../sequelize/models');
require('dotenv').config()

const pool = mysql.createPool({
  connectionLimit: 10,
  multipleStatements: true,
  waitForConnections: true,
  database: process.env.DB_NAMES,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
});

module.exports = (query, options = {}) => new Promise((resolve, reject) => {
 db.sequelize.query(query, options)
  .then(res => {
    resolve(res);
    console.log(res);
  })
  .catch((error) => {
    reject(new Error(`Error while running '${query}: ${error}'`));
  })
});