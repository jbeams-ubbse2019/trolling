const execute = require('../config/db'),
  db = require('../sequelize/models');

exports.getScootersMainList = () => execute('SELECT distinct(kickscooters.id), macAddress, kickscooters.name as kickScooterName, status.name as stateName, number FROM kickscooters JOIN status on status.id = kickscooters.statusId WHERE kickscooters.name LIKE "MIScooter%" OR kickscooters.name LIKE "NBScooter%"',
{
  type: db.Sequelize.QueryTypes.SELECT
});

exports.getScootersMainListByState = (statusName) => execute('SELECT distinct(kickscooters.id), macAddress, kickscooters.name as kickScooterName, number FROM kickscooters WHERE statusId = (SELECT id from status where name = ?)',
{
  replacements: [statusName], 
  type: db.Sequelize.QueryTypes.SELECT
});

exports.getKickscooterToShowById = (ID) => execute('SELECT kickscooters.id, kickscooters.name, macAddress, brand, type, number, status.name as stateName, batteryPercent, distanceLeftKm, odometerKm, serialNr, timeStamp'
+ ' timeStamp  FROM kickscooters JOIN status ON kickscooters.statusId = status.id JOIN kickscooterData ON kickscooterData.kickscooterId ='
+ ' kickscooters.id WHERE kickscooters.id = ? ORDER BY kickscooterData.timeStamp DESC LIMIT 1;',
{
  replacements: [ID], 
  type: db.Sequelize.QueryTypes.SELECT
});

exports.checkIfScooterExists = (ID) => execute('SELECT COUNT(*) as result FROM kickscooters WHERE id = ?',
{
  replacements: [ID], 
  type: db.Sequelize.QueryTypes.SELECT
})
  .then((resp) => resp[0].result > 0);

exports.getAllStateNames = () => execute('select name as stateName from status;',
{ 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.status
});

exports.checkIfKickscooterExists = (ID) => execute('SELECT COUNT(*) as result FROM kickscooters WHERE id = ?; ',
{
  replacements: [ID], 
  type: db.Sequelize.QueryTypes.SELECT
})
  .then((resp) => resp[0].result > 0);

exports.checkIfStateExistsByName = (statName) => execute('SELECT COUNT(*) as result FROM status WHERE name = ?',
{
  replacements: [statName], 
  type: db.Sequelize.QueryTypes.SELECT
})
  .then((resp) => resp[0].result > 0);

exports.updateStateName = (scooterID, statName) => execute('UPDATE kickscooters set statusId = (SELECT id FROM status WHERE name'
  + ' = ?) WHERE kickscooters.id = ? ;',
  {
    replacements: [statName, scooterID], 
    type: db.Sequelize.QueryTypes.UPDATE
  });

exports.getDataByID = (scooterId) => execute('SELECT macAddress, name as kickScooterName, statusId as state_id FROM kickscooters where id = ? ;',
{
  replacements: [scooterId], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.kickscooter
});

exports.getAvailableKickScooters = () =>  execute('SELECT k.id as id, k.brand as brand, k.type as type, macAddress, k.number as number, k.name as kickScooterName, kd.batteryPercent FROM kickscooters AS k ' +
'JOIN (SELECT kickscooterId, batteryPercent FROM kickscooterData WHERE id IN ' + 
'(SELECT MAX(id) FROM kickscooterData GROUP BY kickscooterId)) AS kd ON kd.kickscooterId = k.id ' +
'WHERE statusId = (SELECT id from status where name = "available");',
{ 
  type: db.Sequelize.QueryTypes.SELECT
});

exports.getLastCellVoltages = (scooterID) => execute('SELECT cellVoltage1, cellVoltage2, cellVoltage3, cellVoltage4, cellVoltage5, '
 + 'cellVoltage6, cellVoltage7, cellVoltage8, cellVoltage9, cellVoltage10 '
 + 'FROM cellVoltages AS cv, kickscooterData AS kd '
 + 'WHERE (cv.id = kd.cellVoltageId) and (kd.kickscooterId = ?) ORDER BY kd.timeStamp desc limit 1;',
 {
  replacements: [scooterID], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.cellVoltage
})
  .then((resp) => resp[0]);

exports.batteryTemperatureWithDateInterval = (scooterID, startDate, endDate) => execute('SELECT batteryTemperature, batteryTemperature2, time_stamp ' + 
'FROM kickscooterData WHERE kickscooterId = ? and DATE(timeStamp) BETWEEN ? AND ?;',
{
  replacements: [scooterID, startDate, endDate],
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.kickscooterData
});

exports.getTotalScooterKm = () => execute('SELECT SUM(lastData.km) as kmSum FROM (SELECT kickscooterId, MAX(odometerKm) AS km ' +
'FROM kickscooterData GROUP BY kickscooterId) AS lastData;',
{
  type: db.Sequelize.QueryTypes.SELECT
});

exports.updateBasicKsData = (scooterID, number, brand, type) => execute('UPDATE kickscooters SET number = ?, brand = ?, type = ? WHERE id = ?;',
{
  replacements: [number, brand, type, scooterID],
  type: db.Sequelize.QueryTypes.UPDATE
});
