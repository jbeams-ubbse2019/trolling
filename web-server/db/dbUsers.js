const execute = require('../config/db'),
  db = require('../sequelize/models');

exports.checkIfUserExistsWithMail = (email) => execute('SELECT COUNT(*) as result FROM users WHERE users.email = ?', 
{
  replacements: [email], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
})
  .then((resp) => resp[0].result > 0);

exports.getPassword = (email) => execute('SELECT password as result FROM users WHERE users.email = ?',
{
  replacements: [email], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
})
  .then((resp) => resp[0].result);

exports.getUserRoleByMail = (email) => execute('SELECT roles.name as role FROM users join roles on roles.id = users.roleId WHERE users.email = ?',
{
  replacements: [email], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
})
  .then((resp) => resp[0].role);

exports.insertNewUser = (userData) => execute('INSERT INTO users (firstName, lastName, email, address, password, telNumber, roleId) VALUES (?, ?, ?, ?, ?, ?, (SELECT id from roles where name = "user"));',
{
  replacements: [userData.firstName, userData.lastName, userData.email, userData.address, userData.password, userData.telNumber], 
  type: db.Sequelize.QueryTypes.INSERT
});

exports.getUserId = (email) => execute('SELECT id FROM users WHERE email = ?',
{
  replacements: [email], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
})
  .then((resp) => resp[0].id);

exports.checkIfUserExistsWithID = (userID) => execute('SELECT COUNT(*) as result FROM users WHERE users.id = ?',
{
  replacements: [userID], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
});

exports.checkIfUserDidRent = (userID) => execute('SELECT COUNT(*) AS result from rents where userId = ? AND ISNULL(returnDate);',
{
  replacements: [userID],
  type: db.Sequelize.QueryTypes.SELECT
})
  .then((resp) => resp[0].result);

exports.getUserNameByID = (userID) => execute('SELECT firstName, lastName FROM users WHERE id = ?;',
{
  replacements: [userID], 
  type: db.Sequelize.QueryTypes.SELECT,
  model: db.user
})
  .then((resp) => resp[0]);