const execute = require('../config/db'),
  db = require('../sequelize/models');

exports.getAllForAUser = (userID) => execute('SELECT rents.id, kickscooterId, kickscooters.name as kickScooterName, brand, type, number, macAddress, rentDate FROM rents JOIN kickscooters ON rents.kickscooterId = kickscooters.id WHERE ISNULL(returnDate) AND userId = ?',
{
  replacements: [userID], 
  type: db.Sequelize.QueryTypes.SELECT
});

// rent & bring back a ks
exports.changeRentStatus = (kickscooterId, statusName) => execute('UPDATE kickscooters set statusId = (SELECT id FROM status WHERE name = ?) WHERE id = ? ;',
{
    replacements: [statusName, kickscooterId], 
    type: db.Sequelize.QueryTypes.UPDATE
});

exports.rentAKickscooter = (userID, kickscooterId) => execute('INSERT INTO rents(userId, kickscooterId, rentDate) VALUES (?, ?, CURRENT_TIMESTAMP);', //[kickscooterId, userID, kickscooterId])
{
    replacements: [userID, kickscooterId],
    type: db.Sequelize.QueryTypes.INSERT
});

exports.bringBackAKickscooter = (kickscooterId, rentId) => execute('update rents set returnDate=CURRENT_TIMESTAMP, kmTaken = '
+ '(SELECT odometerKm FROM kickscooterData where kickscooterId = ? ORDER BY timeStamp desc LIMIT 1) - (SELECT odometerKm FROM kickscooterData where kickscooterId = ? ORDER BY timeStamp desc LIMIT 1,1) '
+ 'WHERE id = ?;',
{
  replacements: [kickscooterId, kickscooterId, rentId], 
  type: db.Sequelize.QueryTypes.UPDATE
});

exports.rentsByMonth = () => execute('SELECT MONTH(rentDate) as rentMonth, COUNT(kickscooterId) as nrOfRents '
+ 'FROM rents WHERE TIMESTAMPDIFF(MONTH, rentDate, CURDATE()) < 5 '
+ 'GROUP BY rentMonth ORDER BY rentDate;',
{
  type: db.Sequelize.QueryTypes.SELECT
});

exports.rentDataById = (rentId) => execute('SELECT rentDate, returnDate, kmTaken FROM rents WHERE id = ? ',
{
  replacements: [rentId],
  type: db.Sequelize.QueryTypes.SELECT
});

exports.allPastRentData = (userID) => execute('SELECT rentDate, rentDate as returnedDate, kmTaken as kmPassed, k.number FROM rents AS r JOIN kickscooters AS k ON k.id = r.kickscooterId WHERE userId = ? '
+ 'AND (returnDate IS NOT NULL) ORDER BY rentDate DESC;',
{
  replacements: [userID], 
  type: db.Sequelize.QueryTypes.SELECT
});

exports.rentsByVehicle = () => execute('SELECT k.name AS kickScooterName, r.frequency FROM kickscooters AS k JOIN (SELECT kickscooterId, count(id) AS frequency FROM rents ' +
'GROUP BY kickscooterId) AS r ON r.kickscooterId = k.id',
{
  type: db.Sequelize.QueryTypes.SELECT
});