const express = require('express'),
  morgan = require('morgan'), // loggolni fogja a hivasokat
  session = require('express-session'),
  fs = require('fs'),
  log = require('./config/myLogger');

const requests = require('./routes/requests'),
  users = require('./routes/users'),
  kickscooters = require('./routes/kickscooters'),
  rents = require('./routes/rents');

// database
const db = require('./sequelize/models');

db.sequelize.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(error => console.error('Unable to connect to the database: ', error));

const app = express();

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

// app.use(morgan('tiny'));

app.use(morgan('co', { stream: fs.createWriteStream('./access.log', { flags: 'a' }) }));
app.use(morgan('tiny'));

app.use(express.urlencoded({
  extended: true,
}));


app.use('/requests', requests);
app.use('/users', users);
app.use('/kickscooters', kickscooters);
app.use('/rents', rents);


app.listen(80, () => {
  log.debug('Server listening ... ');
});
