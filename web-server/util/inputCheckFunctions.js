const dbScooter = require('../db/dbKickscooters'),
  log = require('../config/myLogger'),
  dbUsers = require('../db/dbUsers');

exports.checkEmail = (email) => {
  if (!email || email === '') {
    throw new Error('Empty mail address field');
  }
};

exports.checkPasswd = (passwd) => {
  if (!passwd || passwd === '') {
    throw new Error('Empty password field');
  }
};

exports.checkFirstName = (fName) => {
  if (!fName || fName === '') {
    throw new Error('Empty first name field');
  }
  if (!(/^[A-Z]{1}[a-z]+([ -][A-Z]{1}[a-z]+)*$/).test(fName)) {
    throw new Error('Invalid first name format');
  }
};

exports.checkLastName = (lName) => {
  if (!lName || lName === '') {
    throw new Error('Empty last name field');
  }
  if (!(/^[A-Z]{1}[a-z]+([ -][A-Z]{1}[a-z]+)*$/).test(lName)) {
    throw new Error('Invalid last name format');
  }
};

exports.checkEmail = (email) => {
  if (!email || email === '') {
    throw new Error('Empty email field');
  }
  if (!(/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+.[a-z]+$/).test(email)) {
    throw new Error('Invalid email format');
  }
};

exports.checkAddress = (address) => {
  if (!address || address === '') {
    throw new Error('Empty address field');
  }
  if (!address.match(/^[a-zA-Z0-9./\- ]+([ ,][a-zA-Z0-9 ./-]+)*$/)) {
    throw new Error('Invalid address format');
  }
};

function isStrongPwd(pwd) {
  if (!(/^.*[a-z].*$/).test(pwd) || !(/^.*[A-Z].*$/).test(pwd) || !(/^.*[0-9].*$/).test(pwd) || pwd.length < 5) {
    return false;
  }
  return true;
}

exports.checkPasswdSignUp = (pwd, pwd2) => {
  if (!pwd || pwd === '') {
    throw new Error('Empty password field');
  }
  if (!pwd2 || pwd2 === '') {
    throw new Error('Empty confirm password field');
  }
  if (pwd !== pwd2) {
    throw new Error('Passwords do not match');
  }
  if (!isStrongPwd(pwd)) {
    throw new Error('Invalid password format: must contain at least 5 characters, numberic, upper- and lowercase characters');
  }
};

exports.checkIfNumber = (number) => {
  if (!number || number === '') {
    throw new Error('Empty number parameter');
  }
  if (!/^[1-9][0-9]*$/.test(number)) {
    if (number !== '0') {
      throw new Error('The given input is not a number');
    }
  }
};

exports.checkIfScooterExistsByID = (ID) => new Promise((resolve, reject) => {
  dbScooter.checkIfScooterExists(ID)
    .then((result) => {
      if (!result) {
        log.error('There is no existing kickscooter with this id');
        reject(new Error('Invalid kickscooter ID'));
      } else {
        resolve('ok');
      }
    })
    .catch((err) => {
      log.error(err.message);
      reject(new Error('Database Error :( '));
    });
});


exports.checkIfStateExistsByName = (stateName) => new Promise((resolve, reject) => {
  dbScooter.checkIfStateExistsByName(stateName)
    .then((result) => {
      if (!result) {
        log.error('There is no existing kickscooter status with this name');
        reject(new Error('Iinvalid kickscooter status name'));
      } else {
        resolve('ok');
      }
    })
    .catch((err) => {
      log.error(err.message);
      reject(new Error('Database Error :( '));
    });
});

exports.checkIfUserExistsByUserID = (userID) => new Promise((resolve, reject) => {
  dbUsers.checkIfUserExistsWithID(userID)
    .then((result) => {
      if (!result) {
        reject(new Error('Users doesn\'t exists'));
      } else {
        resolve('ok');
      }
    })
    .catch((err) => {
      log.error(err.message);
      reject(new Error('Database Error'));
    });
});

exports.checkIfUserHaveNotRentedYet = (userID) => new Promise((resolve, reject) => {
  dbUsers.checkIfUserDidRent(userID)
    .then((result) => {
      if (result) {
        reject(new Error('You have already rented a vehicle.'));
      } else {
        resolve('ok');
      }
    })
    .catch((err) => {
      log.error(err.message);
      reject(new Error('Database Error'));
    });
});

exports.checkDateInterval = (beginDate, endDate) => {
  const d1 = new Date(beginDate);
  const d2 = new Date(endDate);
  if (!beginDate || beginDate === '') {
    throw new Error('No begin date input!');
  }
  if (!endDate || endDate === '') {
    throw new Error('No end date input!');
  }
  if (d1.getTime() > d2.getTime()) {
    throw new Error('The begin date must be earlier than end date!');
  }
};

exports.checkTelNumber = (telNum) => {
  if (!telNum || telNum === '') {
    throw new Error('Empty telephone number field');
  }
  if (!(/^\+{0,1}[0-9]{10,15}$/).test(telNum)) {
    throw new Error('Invalid telephone number format');
  }
};
