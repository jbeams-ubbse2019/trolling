const crypto = require('crypto'),
  log = require('../config/myLogger');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

exports.hashPasswd = (passwd) => new Promise((resolve, reject) => {
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) {
      reject(new Error(err));
    }
    crypto.pbkdf2(passwd, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        log.error(`Hashing unsuccessful: ${cryptErr.message}`);
        reject(new Error('Hashing unsuccessful'));
      } else {
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        // a konkatenált hash-t és sót tárolnánk adatbázisban
        resolve(hashWithSalt);
      }
    });
  });
});

exports.deHashPasswd = (password, hashWithSalt) => new Promise(
  (resolve, reject) => {
    const expectedHash = hashWithSalt.substring(0, hashSize * 2),
      salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');

    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
      if (err) {
        log.error(`Hashing unsuccessful: ${err.message}`);
        reject(new Error('Internal server error'));
      } else {
        const actualHash = binaryHash.toString('hex');
        if (expectedHash !== actualHash) {
          log.error('Wrong password');
          reject(new Error('Wrong username or password'));
        } else {
          resolve('ok');
        }
      }
    });
  },
);
