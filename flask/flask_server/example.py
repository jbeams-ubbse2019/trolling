""" The routs for the webserver endpoints"""
import os
import subprocess

from flask_server import APP
from flask import request, jsonify

@APP.route("/kickscooter/data", methods=['PUT'])
def search_for_mac_address():
    """ Get request to collect all the data from the kickscooters """
    os.system("python3 /home/pi/trolling/python/search_mac_addresses.py")
    return "ok"

@APP.route("/kickscooter/updateStatus", methods=['PUT'])   #list kickscooters page
def update_states():
    """ PUT methode to update kickscooters state, check whici is available and whicj is not"""
    os.system("python3 /home/pi/trolling/python/update_scooter_state.py")
    return "ok"

@APP.route("/kickscooter/insertAll", methods=['PUT'])   #list kickscooters page
def insert_all_ks():
    os.system("python3 /home/pi/trolling/python/insert_all_new_scooter.py")
    return "ok"

@APP.route("/kickscooter/data", methods=['GET'])
def insert_data_from_one():
    address = request.args['address']
    name = request.args['name']
    os.system("python3 /home/pi/trolling/python/get_data.py {} {} ".format(address, name))
    return "ok"

@APP.route("/kickscooter/lock", methods=['POST'])
def lock_scooter():
    """ POST request to lock a kickscooter"""
    address = request.args['address']
    name = request.args['name']
    os.system("python3 /home/pi/trolling/python/lock_scooter.py {} {} ".format(address, name))
    return "ok"

@APP.route("/kickscooter/unlock", methods=['POST'])
def unlock_scooter():
    """ POST request to unlock a kickscooter """
    address = request.args['address']
    name = request.args['name']
    os.system("python3 /home/pi/trolling/python/unlock_scooter.py {} {} ".format(address, name))
    return "ok"

@APP.route("/kickscooter/light_on", methods=['POST'])
def light_on():
    """ POST request to turn on the tail light of a kickscooter"""
    address = request.args['address']
    name = request.args['name']
    os.system("python3 /home/pi/trolling/python/tail_light_on.py {} {} ".format(address, name))
    return "ok"

@APP.route("/kickscooter/light_off", methods=['POST'])
def light_off():
    """ POST request to turn off the tail light of a kickscooter """
    address = request.args['address']
    name = request.args['name']
    os.system("python3 /home/pi/trolling/python/tail_light_off.py {} {} ".format(address, name))
    return "ok"

@APP.route("/kickscooter/status", methods=['PUT']) #kickscooter status
def light_lock_status():
    """ PUT request to get the lock and light states of a kickscooter"""
    address = request.args['address']
    name = request.args['name']
    # pylint: disable = line-too-long
    output = subprocess.check_output(["python3", "/home/pi/trolling/python/tail_light_lock_status.py", address, name])
    aux = output.decode("utf8").replace("'", '"')
    return jsonify(aux)
