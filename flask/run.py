"""The script to start the flask webserver"""
from flask_server import APP

if __name__ == '__main__':
    APP.run(debug=True, host='0.0.0.0')
