"""This module is locks a kickscooter, his first argument is the device MAC address"""
import sys
import time

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

if SCOOTER_NAME.startswith("NBScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
    UNLOCK = ninebot_message.UNLOCK_SCOOTER
elif SCOOTER_NAME.startswith("MIScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
    UNLOCK = m365message.UNLOCK_SCOOTER

SCOOTER.connect()

SCOOTER.request(UNLOCK)

SCOOTER.disconnect()

time.sleep(3)
sys.exit(0)
