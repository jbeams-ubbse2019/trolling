from __future__ import print_function

import os

from bluepy.btle import Scanner
from configparser import ConfigParser

import mysql.connector

PARSER = ConfigParser()
PARSER.read('/home/pi/trolling/database/config.ini')

MY_HOST = PARSER.get("db", "host")
MY_USERNAME = PARSER.get("db", "user")
MY_PASSWD = PARSER.get("db", "passwd")
DATABASE_NAME = PARSER.get("db", "database")

MY_DB = mysql.connector.connect(
    host = MY_HOST,
    user = MY_USERNAME,
    passwd = MY_PASSWD,
    database = DATABASE_NAME
)

# Scans for available devices.
SCAN = Scanner()
SEC = 5
ADDRESS = ""

DEVS = SCAN.scan(SEC)
for dev in DEVS:
    LOCALNAME = dev.getValueText(9)
    if LOCALNAME and (LOCALNAME.startswith("MIScooter") or LOCALNAME.startswith("NBScooter")):
        print(LOCALNAME)
        MY_CURSOR = MY_DB.cursor()

        DATA = (dev.addr,
            LOCALNAME, 0, "")

        print(MY_CURSOR.callproc("insert_foreign_kickscooter", DATA))
        MY_CURSOR.execute('COMMIT')
        
