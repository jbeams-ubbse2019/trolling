""" Gets a MAC address as a parameter and sets the roller available in the database"""
import sys

from configparser import ConfigParser

import mysql.connector

PARSER = ConfigParser()
PARSER.read('/home/pi/trolling/database/config.ini')

MY_HOST = PARSER.get("db", "host")
MY_USERNAME = PARSER.get("db", "user")
MY_PASSWD = PARSER.get("db", "passwd")
DATABASE_NAME = PARSER.get("db", "database")

MY_DB = mysql.connector.connect(
    host = MY_HOST,
    user = MY_USERNAME,
    passwd = MY_PASSWD,
    database = DATABASE_NAME
)

#-------------------------------
"""
MY_CURSOR = MY_DB.cursor()

DATA = (sys.argv[1],
        sys.argv[2])

print(MY_CURSOR.callproc("insert_foreign_kickscooter", DATA))
MY_CURSOR.execute('COMMIT')
"""
#-------------------------------

MY_CURSOR = MY_DB.cursor()

ARG_LEN = len(sys.argv)
COMMAND = "update kickscooters SET statusId = (SELECT status.id FROM status WHERE name = 'available') WHERE macAddress = "

MY_COMMAND  = COMMAND + "'" + sys.argv[1] + "'" + "AND statusId = (SELECT status.id FROM status WHERE name = 'not found');"
MY_CURSOR.execute(MY_COMMAND)

MY_CURSOR.execute('COMMIT')

MY_DB.disconnect()
