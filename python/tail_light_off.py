"""This module is locks a kickscooter, his first argument is the device MAC address"""
import sys
import time

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

if SCOOTER_NAME.startswith("NBScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
    LIGHT_OFF = ninebot_message.TAIL_LIGHT_OFF
elif SCOOTER_NAME.startswith("MIScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
    LIGHT_OFF = m365message.TAIL_LIGHT_OFF

SCOOTER.connect()

# unlock scooter
SCOOTER.request(LIGHT_OFF)
# fetch value to confirm that the scooter is unlocked

SCOOTER.disconnect()

time.sleep(3)
sys.exit(0)
