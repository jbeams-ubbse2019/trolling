"""This module is locks a kickscooter, his first argument is the device MAC address"""
import sys
import time

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

if SCOOTER_NAME.startswith("NBScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
    LIGHT_ON = ninebot_message.TAIL_LIGHT_ON
elif SCOOTER_NAME.startswith("MIScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
    LIGHT_ON = m365message.TAIL_LIGHT_ON

SCOOTER.connect()

SCOOTER.request(LIGHT_ON)

SCOOTER.disconnect()

time.sleep(3)
sys.exit(0)
