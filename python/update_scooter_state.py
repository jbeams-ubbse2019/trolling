"""
This script calls a script that sets all scooters unavailable, and then scans for
mac addresses and sets them to available
"""
from __future__ import print_function

import os

from bluepy.btle import Scanner

# Scans for available devices.
SCAN = Scanner()
SEC = 5
ADDRESS = ""

os.system("python3 /home/pi/trolling/python/set_scooters_unavailable.py")


DEVS = SCAN.scan(SEC)
for dev in DEVS:
    LOCALNAME = dev.getValueText(9)
    if LOCALNAME and (LOCALNAME.startswith("MIScooter") or LOCALNAME.startswith("NBScooter")):
        print(LOCALNAME)
        os.system("python3 /home/pi/trolling/python/set_scooters_available.py '" +  dev.addr + "' " + " '" + LOCALNAME + "' ")
