"""Constructs the messages for the Xiaomi electric kickscooters"""

import struct

HEADER = 0xAA55

class Direction():
    """Possible destinations of the messages"""
    # MASTER_TO_MOTOR      = 0x20
    # MASTER_TO_BATTERY    = 0x22
    # MOTOR_TO_MASTER      = 0x23
    # BATTERY_TO_MASTER    = 0x25

    REQ_TO_ESC = 0x20     # electronic stability control
    REQ_TO_BLE = 0x21
    REQ_TO_BMS = 0x22     # Battery Management System
    REQ_TO_EXTERN_BMS = 0x23

class ReadWrite():
    """The ReadWrite components possible values"""
    READ  = 0x01
    WRITE = 0x03

class Attribute():
    """The attribute components possible values"""
    # BATTERY_INFO           = 0x30 # remaining cap, percent, current, voltage, temperature
    # BATTERY_CELL_VOLTAGES  = 0x40 # cell voltages (Cells 1-10)
    # SERIAL_NUMBER          = 0x10
    # TOTAL_UPTIME           = 0x1a
    CELL_VOLTAGES          = 0x40
    BATTERY_INFO           = 0x30
    SERIAL_NUMBER          = 0x10
    TOTAL_KILOMETER        = 0xb7
    LOCK                   = 0x70
    UNLOCK                 = 0x71
    CRUISE_CONTROL_ON      = 0x7c
    TAIL_LIGHT             = 0x7d
    KERS_KRUISE_TAIL_LIGHT = 0x7b
    SCOOTER_MODE           = 0x72
    BLE_VERSION            = 0x67
    UPTIME                 = 0xb0
    TOTAL_UPTIME           = 0x1a

class ParseStatus:
    """Possible parse states"""
    OK               = 'ok'
    DISJOINTED       = 'disjointed'
    INVALID_HEADER   = 'invalid_header'
    INVALID_CHECKSUM = 'invalid_checksum'

class Message:
    """class that contains all the necessary methodes to construct the message"""

    def __init__(self):
        self.direction  = None
        self.read_write = None
        self.attribute  = None
        self.payload    = None

        self._checksum  = None
        self._raw_bytes  = None

    def set_direction(self, direction):
        """sets the directions value"""
        self.direction = direction
        return self

    def set_read_write(self, read_write):
        """sets the read writes value"""
        self.read_write = read_write
        return self

    def set_attribute(self, attribute):
        """Set the attributes value"""
        self.attribute = attribute
        return self

    # For now payload is stored in little endian format
    def set_payload(self, payload):
        """Sets the payload"""
        self.payload = payload
        return self

    def _calc_checksum(self):
        """calculates the checksum"""
        checksum = 0
        checksum += self.direction
        checksum += self.read_write
        checksum += self.attribute

        # NOTE: python2x and python3x does not have compliant byte literals
        try:
            # python 2.7
            for byte in self.payload:
                byte_val = struct.unpack('>B', byte)[0]
                checksum += byte_val
        except Exception:
            # python 3.x
            for byte_val in self.payload:
                checksum += byte_val

        checksum += len(self.payload) + 2
        checksum ^= 0xffff
        checksum &= 0xffff

        self._checksum = checksum

    def build(self):
        """Builds the message"""
        self._calc_checksum()

        result = bytearray()
        result.extend(struct.pack('<H', HEADER))
        # >>>> these are single byte so we don't have to worry about byte order
        result.append(len(self.payload) + 2)
        result.append(self.direction)
        result.append(self.read_write)
        result.append(self.attribute)

        result.extend(self.payload)
        result.extend(struct.pack('<H', self._checksum))
        self._raw_bytes = bytes(result)
        return self

    @staticmethod
    def parse_from_bytes(message):
        """Constructs the message from the smaller parts"""
        message_length = len(message)
        payload_start = 6
         # pylint: disable=line-too-long
        header, length, direction, read_write, attribute = struct.unpack('<HBBBB', message[:payload_start])
        payload_end = payload_start + length - 2
        total_length = payload_start + length

        if header != HEADER:
            return ParseStatus.INVALID_HEADER, None
        if total_length != message_length:
            return ParseStatus.DISJOINTED, None

        payload    = message[payload_start:payload_end]

        result = Message()              \
            .set_direction(direction)   \
            .set_read_write(read_write) \
            .set_attribute(attribute)   \
            .set_payload(payload)       \
            .build()

        if message[:-2] !=  result._raw_bytes[:-2]:
            return ParseStatus.INVALID_CHECKSUM, None

        return ParseStatus.OK, result

BATTERY_VOLTAGE = Message()                           \
    .set_direction(Direction.REQ_TO_BMS)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.CELL_VOLTAGES)           \
    .set_payload(b'\x18')                             \
    .build()

BATTERY_INFO = Message()                              \
    .set_direction(Direction.REQ_TO_BMS)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.BATTERY_INFO)            \
    .set_payload(b'\x18')                             \
    .build()

BATTERY_SERIAL_NUMBER = Message()                     \
    .set_direction(Direction.REQ_TO_BMS)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.SERIAL_NUMBER)           \
    .set_payload(b'\x22')                             \
    .build()

VEHICLE_SERIAL_NUMBER = Message()                     \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.SERIAL_NUMBER)           \
    .set_payload(b'\x2c')                             \
    .build()

TOTAL_KILOMETER = Message()                           \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.TOTAL_KILOMETER)         \
    .set_payload(b'\x18')                             \
    .build()

LOCK_SCOOTER = Message()                              \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.LOCK)                    \
    .set_payload(b'\x01')                             \
    .build()

UNLOCK_SCOOTER = Message()                            \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.UNLOCK)                  \
    .set_payload(b'\x01')                             \
    .build()

CRUISE_ON = Message()                                 \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.CRUISE_CONTROL_ON)       \
    .set_payload(b'\x01')                             \
    .build()

CRUISE_OFF = Message()                                \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.CRUISE_CONTROL_ON)       \
    .set_payload(b'\x00')                             \
    .build()

TAIL_LIGHT_ON = Message()                             \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.TAIL_LIGHT)              \
    .set_payload(b'\x02\x00')                         \
    .build()

TAIL_LIGHT_OFF = Message()                            \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.WRITE)                  \
    .set_attribute(Attribute.TAIL_LIGHT)              \
    .set_payload(b'\x00\x00')                         \
    .build()

GET_IF_KERS_CRUISE_TAIL_IS_ON = Message()             \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.KERS_KRUISE_TAIL_LIGHT)  \
    .set_payload(b'\x06')                             \
    .build()

SCOOTER_MODE = Message()                              \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.SCOOTER_MODE)            \
    .set_payload(b'\x20')                             \
    .build()

BLE_VERSION = Message()                               \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.BLE_VERSION)             \
    .set_payload(b'\x32')                             \
    .build()

UPTIME = Message()                                    \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.UPTIME)                  \
    .set_payload(b'\x1c')                             \
    .build()

TOTAL_UPTIME_AND_LEFT_KM = Message()                  \
    .set_direction(Direction.REQ_TO_ESC)              \
    .set_read_write(ReadWrite.READ)                   \
    .set_attribute(Attribute.TOTAL_UPTIME)            \
    .set_payload(b'\x46')                             \
    .build()
