"""Here are defined the messages and the functions to process the response"""
from collections import namedtuple

import struct
import logging
import binascii

from bluepy.btle import DefaultDelegate
from .m365message import Message, ParseStatus, Attribute

LOG = logging.getLogger('m365py')
LOG.setLevel(logging.ERROR)

def phex(my_string):
    """ a binaris adatokat atalakitja hexadecimalis formaban"""
    return binascii.hexlify(my_string)

class KersMode():
    """Values of Kers Modes"""
    WEAK   = 0x00
    MEDIUM = 0x01
    STRONG = 0x02

class ScooterMode():
    """Constants for the scooter functioning modes"""
    DRIVE   = 0x00
    ECO     = 0x01

class M365Delegate(DefaultDelegate):
    """ Handles all the messages"""
    def __init__(self, m365):
        DefaultDelegate.__init__(self)
        self._m365 = m365
        self._disjointed_messages = []

    @staticmethod
    def unpack_to_dict(fields, unpacked_tuple):
        """unpacks the dictionarry"""
        result = namedtuple('namedtuple', fields)
        result = result._make(unpacked_tuple) # insert unpacked values
        result = result._asdict()             # convert to OrderedDict
        result = dict(result)                 # convert to regular dict
        return result

    def handle_message(self, message):
        """Handles the responses"""
        LOG.debug("Received message: %s", message.__dict__)
        LOG.debug("Payload: %s", phex(message.payload))

        result = {}

        if message.attribute == Attribute.CELL_VOLTAGES:
            cell_voltages_tuple = struct.unpack('<HHHHHHHHHH', message.payload[:20])

            result['cell_voltages'] = []

            for voltage in cell_voltages_tuple:
                result['cell_voltages'].append(float(voltage) / 100) # V

        elif message.attribute == Attribute.BATTERY_INFO:
            result = M365Delegate.unpack_to_dict(
                'remaining_capacity battery_percent battery_current ' +
                'battery_voltage battery_temperature_1 battery_temperature_2',
                struct.unpack('<HHHHBB', message.payload[2:12])
            )

        elif message.attribute == Attribute.SERIAL_NUMBER and len(message.payload) == 0x22:
            # after the serial number maybe there is the BMS firmware version
            # the designed capacity it's not sure, I dont have enough device to test
            result = M365Delegate.unpack_to_dict(
                'battery_serial_number bms_version designed_capacity full_charges' +
                ' total_number_of_charges',
                struct.unpack('<14sHH4xHH', message.payload[:26])
            )

        elif message.attribute == Attribute.SERIAL_NUMBER and len(message.payload) == 0x2c:
            # maybe there is the ESC firmware version
            result = M365Delegate.unpack_to_dict(
                'vehicle_serial_number pin esc_version',
                struct.unpack('<14s6sH', message.payload[:22])
            )

        elif message.attribute == Attribute.TOTAL_UPTIME:
            result = M365Delegate.unpack_to_dict(
                'km_left trip_distance total_uptime trip_time',
                struct.unpack('<22xH18xH4xl14xH', message.payload[:68])
            )

        elif message.attribute == Attribute.TOTAL_KILOMETER:
            result = M365Delegate.unpack_to_dict(
                'total_km',
                struct.unpack('<l', message.payload[:4])
            )

        elif message.attribute == Attribute.KERS_KRUISE_TAIL_LIGHT:
            result = M365Delegate.unpack_to_dict(
                'kers_mode is_cruise_on is_tail_light_on',
                struct.unpack('<HHH', message.payload)
            )

        elif message.attribute == Attribute.SCOOTER_MODE:
            result = M365Delegate.unpack_to_dict(
                'scooter_mode',
                struct.unpack('<6xh', message.payload[:8])
            )

        elif message.attribute == Attribute.BLE_VERSION:
            result = M365Delegate.unpack_to_dict(
                'ble_version',
                struct.unpack('<2xH', message.payload[:4])
            )

        elif message.attribute == Attribute.UPTIME:
            result = M365Delegate.unpack_to_dict(
                'is_lock_on speed uptime',
                struct.unpack('<4xB5xh8xH', message.payload[:22])
            )

        elif message.attribute == Attribute.TOTAL_UPTIME:
            result = M365Delegate.unpack_to_dict(
                'km_left trip_distance total_uptime trip_time',
                struct.unpack('<22xH18xH4xl14xH', message.payload[:68])
            )

        else:
            LOG.warning('Unhandled message!')
            return

        def try_update_field(result, key, func):
            if key in result:
                new_val = func(result[key])
                result[key] = new_val

        # Convert raw bytes to corresponing type
        try_update_field(result, 'trip_distance',         lambda x: float(x) / 100) # km
        try_update_field(result, 'km_left',               lambda x: float(x) / 100)  # km
        try_update_field(result, 'total_km',              lambda x: float(x) / 1000) # km
        # try_update_field(result, 'battery_percent',       lambda x: '{}%'.format(x))
        try_update_field(result, 'remaining_capacity',    lambda x: float(x) / 1000) # Ah
        try_update_field(result, 'battery_current',       lambda x: float(x) / 100)  # A
        try_update_field(result, 'battery_voltage',       lambda x: float(x) / 100)  # V
        try_update_field(result, 'battery_temperature_1', lambda x: x - 20)   # C
        try_update_field(result, 'battery_temperature_2', lambda x: x - 20)   # C
        try_update_field(result, 'is_tail_light_on',      lambda x: x == 0x02)   # bool
        try_update_field(result, 'is_lock_on',            lambda x: x == 0x02)   # bool
        try_update_field(result, 'is_cruise_on',          lambda x: x == 0x01)   # bool
        try_update_field(result, 'speed',                 lambda x: float(x) / 1000)   # km/h
        try_update_field(result, 'battery_serial_number', lambda x: x.decode("utf-8"))
        try_update_field(result, 'vehicle_serial_number', lambda x: x.decode("utf-8"))
        try_update_field(result, 'pin',                   lambda x: x.decode("utf-8"))


        if 'esc_version' in result:
            result['esc_version'] = '{:02x}'.format(result['esc_version'])
            result['esc_version'] = 'V' + '.'.join(result['esc_version'])

        if 'bms_version' in result:
            result['bms_version'] = '{:02x}'.format(result['bms_version'])
            result['bms_version'] = 'V' + '.'.join(result['bms_version'])

        if 'ble_version' in result:
            result['ble_version'] = '{:02x}'.format(result['ble_version'])
            result['ble_version'] = 'V' + '.'.join(result['ble_version'])

        if 'kers_mode' in result:
            my_val = result['kers_mode']
            if my_val == KersMode.WEAK:
                result['kers_mode'] = 'Weak'
            elif my_val == KersMode.MEDIUM:
                result['kers_mode'] = 'Medium'
            elif my_val == KersMode.STRONG:
                result['kers_mode'] = 'Strong'

        if 'scooter_mode' in result:
            my_val = result['scooter_mode']
            if my_val == ScooterMode.DRIVE:
                result['scooter_mode'] = 'Drive'
            elif my_val == ScooterMode.ECO:
                result['scooter_mode'] = 'Eco'

        # write result to m365 cached state
        for key, value in result.items():
            # hacky way of writing key and value to state
            self._m365.cached_state[key] = value

        # call user callback
        if self._m365._callback:
            self._m365._callback(self._m365, message, result)


    def handleNotification(self, cHandle, data):
        """Transforms the response into correct format"""
        data = bytes(data)
        LOG.debug('Got raw bytes: %s', phex(data))

        # sometimes we receive empty payload, ignore these
        if not data:
            return

        combined_data = bytearray()
        for i, prev_data in enumerate(self._disjointed_messages):
            combined_data.extend(prev_data)

        combined_data.extend(data)
        combined_data = bytes(combined_data)


        parse_status, message = Message.parse_from_bytes(combined_data)

        if parse_status == ParseStatus.OK:
            self.handle_message(message)
            for i, _ in enumerate(self._disjointed_messages):
                del self._disjointed_messages[i]
            self._disjointed_messages = []

        elif parse_status == ParseStatus.DISJOINTED:
            self._disjointed_messages.append(data)

        elif parse_status == ParseStatus.INVALID_HEADER:
            LOG.warning('Received packet with invalid header')

        elif parse_status == ParseStatus.INVALID_CHECKSUM:
            LOG.warning('Received packet with invalid checksum')
