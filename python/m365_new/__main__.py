"""the modules task is to communicate, send some specific message to the Ninebot kickscooter"""
#versions
from bluepy.btle import Scanner

# Scans for available devices.
SCAN = Scanner()
SEC = 5
print("Scanning for %s seconds", SEC)
DEVS = SCAN.scan(SEC)
print("Scooters found:")
for dev in DEVS:
    localname = dev.getValueText(9)
    if localname and localname.startswith("MIScooter"):
        print("  %s, addr=%s, rssi=%d" % (localname, dev.addr, dev.rssi))
