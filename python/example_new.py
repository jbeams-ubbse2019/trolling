"""Example script about the Ninebot protokoll usage"""
import time
import json

import logging

from ble_connection import connect
from m365_new import m365message

# this sets debug level of m365py
logging.getLogger('m365py_new').setLevel(logging.DEBUG)

SCOOTER_MAC_ADDRESS = 'D0:D8:3D:52:39:ff'
SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
SCOOTER.connect()

UPDATE_INTERVAL_S = 5.0
while True:
    START_TIME = time.time()

    # Request all currently supported 'attributes'
    SCOOTER.request(m365message.BATTERY_VOLTAGE)
    SCOOTER.request(m365message.BATTERY_INFO)
    SCOOTER.request(m365message.BATTERY_SERIAL_NUMBER)
    SCOOTER.request(m365message.VEHICLE_SERIAL_NUMBER)
    SCOOTER.request(m365message.TOTAL_KILOMETER)
    SCOOTER.request(m365message.GET_IF_KERS_CRUISE_TAIL_IS_ON)
    SCOOTER.request(m365message.SCOOTER_MODE)
    SCOOTER.request(m365message.BLE_VERSION)
    SCOOTER.request(m365message.UPTIME)
    SCOOTER.request(m365message.TOTAL_UPTIME_AND_LEFT_KM)

    # SCOOTER.request(m365message.LOCK_SCOOTER)
    # SCOOTER.request(m365message.UNLOCK_SCOOTER)

    # SCOOTER.request(m365message.CRUISE_ON)
    # SCOOTER.request(m365message.CRUISE_OFF)

    # SCOOTER.request(m365message.TAIL_LIGHT_ON)
    # SCOOTER.request(m365message.TAIL_LIGHT_OFF)

    # m365py also stores a cached state of received values
    print(json.dumps(SCOOTER.cached_state, indent=4, sort_keys=True))
    # try to consistently run loop every 5 seconds
    ELAPSED_TIME = time.time() - START_TIME
    SLEEP_TIME = max(UPDATE_INTERVAL_S - ELAPSED_TIME, 0)
    time.sleep(SLEEP_TIME)

SCOOTER.disconnect()
