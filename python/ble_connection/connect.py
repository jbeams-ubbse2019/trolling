"""This module is responsible to connect to the kickscooters with BLE"""
import logging
import binascii

from bluepy.btle import Peripheral, UUID, ADDR_TYPE_RANDOM
from m365_new import m365py
from ninebot import ninebotpy

LOG = logging.getLogger('connect')
LOG.setLevel(logging.ERROR)

def phex(string):
    """ a binaris adatokat atalakitja hexadecimalis formaban"""
    return binascii.hexlify(string)

class Connect(Peripheral):
    """The class contains the necessary methodes to manage the whole communication"""
    RX_CHARACTERISTIC = UUID('6e400003-b5a3-f393-e0a9-e50e24dcca9e')
    TX_CHARACTERISTIC = UUID('6e400002-b5a3-f393-e0a9-e50e24dcca9e')

    def __init__(self, mac_address, scooter_type, callback=None,
                 auto_reconnect=True):
        Peripheral.__init__(self)
        self.mac_address = mac_address
        self._auto_reconnect = auto_reconnect
        self._scooter_type = scooter_type

        self._tx_char = None
        self._rx_char = None
        self._all_characteristics = None
        self.cached_state = {}
        self._callback = callback
        self._disconnected_callback = None
        self._connected_callback = None

        stream_handler = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stream_handler.setFormatter(formatter)

        LOG.addHandler(stream_handler)

    def set_connected_callback(self, connected_callback):
        """Sets the callback, which will be called after the connection was established"""
        self._connected_callback = connected_callback

    def set_disconnected_callback(self, disconnected_callback):
        """Sets callback, which will be called after disconnect """
        self._disconnected_callback = disconnected_callback

    @staticmethod
    def _find_characteristic(uuid, chars):
        results = filter(lambda x: x.uuid == uuid, chars)
        for result in results:  # return the first match
            return result
        return None

    def _try_connect(self):
        LOG.info('Attempting to connect to Scooter: %s', self.mac_address)
        while True:
            try:
                Peripheral.connect(self, self.mac_address, addrType=ADDR_TYPE_RANDOM)
                LOG.info('Successfully connected to Scooter: %s', self.mac_address)
                if self._connected_callback:
                    self._connected_callback(self)

                # Attach delegate
                self.withDelegate(self._set_scooter_delegate_type())

                # Turn on notifications, otherwise there won't be any notification
                self.writeCharacteristic(0xc, b'\x01\x00', True)
                self.writeCharacteristic(0x12, b'\x01\x00', True)

                self._all_characteristics = self.getCharacteristics()

                self._tx_char = Connect._find_characteristic(Connect.TX_CHARACTERISTIC,
                                                             self._all_characteristics)
                self._rx_char = Connect._find_characteristic(Connect.RX_CHARACTERISTIC,
                                                             self._all_characteristics)
                break

            except Exception as my_exception:
                if self._auto_reconnect:
                    LOG.warning('%s , retrying', my_exception)
                else:
                    raise my_exception

    def _set_scooter_delegate_type(self):
        if self._scooter_type == "Ninebot":
            return ninebotpy.NinebotDelegate(self)

        if self._scooter_type == "Xiaomi":
            return m365py.M365Delegate(self)

        return None

    def _try_reconnect(self):
        try:
            self.disconnect()
        except Exception:
            pass
        if self._disconnected_callback:
            self._disconnected_callback(self)
        self._try_connect()

    def connect(self, mac_address=None):
        """Public function to begin connecting to the given MAC address"""
        if mac_address:
            self.mac_address = mac_address
        self._try_connect()

    def request(self, message):
        """Requests the given message"""
        while True:
            try:
                LOG.debug('Sending message: %s', [v for (k, v) in message.__dict__.items()])
                LOG.debug('Sending bytes: %s', phex(message._raw_bytes))

                self._tx_char.write(message._raw_bytes)
                self._rx_char.read()

                break
            except Exception as my_error:
                if self._auto_reconnect:
                    LOG.warning('%s , reconnecting', my_error)
                    self._try_reconnect()
                else:
                    raise my_error

# def wait_for_notifications(self, timeout):
#     # Waits the given timeout for notification
#     try:
#         Peripheral.wait_for_notifications(self, timeout)
#     except Exception as my_error:
#         if self._auto_reconnect:
#             LOG.warning('%s, reconnecting', my_error)
#             self._try_reconnect()
#         else:
#             raise my_error
