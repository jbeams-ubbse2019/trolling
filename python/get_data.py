""" This module gets all the implmented data from the kicksccoter,
you gave him MAC addresses as arguments"""
from __future__ import print_function

import json
import sys
import os

# this sets debug level of m365py
import logging

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

logging.getLogger('get_data').setLevel(logging.ERROR)

def get_data_ninebot(scooter):
    """Mines the data from the kickscooter"""
    scooter.connect()

    for _ in range(3):
        # Request all currently supported 'attributes'
        scooter.request(ninebot_message.BATTERY_VOLTAGE)
        scooter.request(ninebot_message.BATTERY_INFO)
        scooter.request(ninebot_message.BATTERY_SERIAL_NUMBER)
        scooter.request(ninebot_message.VEHICLE_SERIAL_NUMBER)
        scooter.request(ninebot_message.TOTAL_KILOMETER)
        scooter.request(ninebot_message.GET_IF_KERS_CRUISE_TAIL_IS_ON)
        scooter.request(ninebot_message.SCOOTER_MODE)
        scooter.request(ninebot_message.BLE_VERSION)
        scooter.request(ninebot_message.UPTIME)
        scooter.request(ninebot_message.TOTAL_UPTIME_AND_LEFT_KM)

    scooter.disconnect()

def get_data_m365(scooter):
    """Mines the data from the kickscooter"""
    scooter.connect()

    for _ in range(3):
        # Request all currently supported 'attributes'
        scooter.request(m365message.BATTERY_VOLTAGE)
        scooter.request(m365message.BATTERY_INFO)
        scooter.request(m365message.BATTERY_SERIAL_NUMBER)
        scooter.request(m365message.VEHICLE_SERIAL_NUMBER)
        scooter.request(m365message.TOTAL_KILOMETER)
        scooter.request(m365message.GET_IF_KERS_CRUISE_TAIL_IS_ON)
        scooter.request(m365message.SCOOTER_MODE)
        scooter.request(m365message.BLE_VERSION)
        scooter.request(m365message.UPTIME)
        scooter.request(m365message.TOTAL_UPTIME_AND_LEFT_KM)

    scooter.disconnect()

i = 1
ARG_LENGTH = len(sys.argv)

while i < ARG_LENGTH:

    SCOOTER_MAC_ADDRESS = sys.argv[i]
    SCOOTER_NAME = sys.argv[i+1]
    i += 2
    SUCCES = 0
    if SCOOTER_NAME.startswith("NBScooter"):
        MY_SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
        get_data_ninebot(MY_SCOOTER)
    elif SCOOTER_NAME.startswith("MIScooter"):
        MY_SCOOTER = SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
        get_data_m365(MY_SCOOTER)


    F = open("json_data.txt", "w")
    F.write(json.dumps(MY_SCOOTER.cached_state))
    F.close()
    os.system("python3  /home/pi/trolling/database/insert_into_database.py " +
              SCOOTER_MAC_ADDRESS + " " + SCOOTER_NAME)
