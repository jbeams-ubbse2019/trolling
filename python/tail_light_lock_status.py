"""This module is locks a kickscooter, his first argument is the device MAC address"""
import sys
import json

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

if SCOOTER_NAME.startswith("NBScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
    TAIL_LIGHT_STATUS = ninebot_message.GET_IF_KERS_CRUISE_TAIL_IS_ON
    LOCK_STATUS = ninebot_message.UPTIME
elif SCOOTER_NAME.startswith("MIScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
    TAIL_LIGHT_STATUS = m365message.GET_IF_KERS_CRUISE_TAIL_IS_ON
    LOCK_STATUS = m365message.UPTIME

SCOOTER.connect()

SCOOTER.request(TAIL_LIGHT_STATUS)
SCOOTER.request(TAIL_LIGHT_STATUS)
SCOOTER.request(LOCK_STATUS)
SCOOTER.request(LOCK_STATUS)

SCOOTER.disconnect()

RESPONSE = {'tail_light': SCOOTER.cached_state["is_tail_light_on"], 'lock': SCOOTER.cached_state["is_lock_on"]}
print(json.JSONEncoder().encode(RESPONSE))

sys.exit(0)
