"""Constructs the messages for the Ninebot electric kickscooters"""
import struct

HEADER = 0xA55A

class Destination:
    """Possible destinations of the messages"""
    REQ_TO_ESC        = 0x20     # electronic stability control
    REQ_TO_BLE        = 0x21
    REQ_TO_BMS        = 0x22     # Battery Management System
    REQ_TO_EXTERN_BMS = 0x23

class Source:   # no difference I think
    """Possible sources of the messages"""
    APPLICATION1 = 0x3D
    APPLICATION2 = 0x3E
    APPLICATION3 = 0x3F

class Command:
    """The command components possible values"""
    READ = 0x01
    WRITE = 0x02

class ParseStatus:
    """Possible parse states"""
    OK               = 'ok'
    DISJOINTED       = 'disjointed'
    INVALID_HEADER   = 'invalid_header'
    INVALID_CHECKSUM = 'invalid_checksum'

class Argument:
    """Command arguments"""
    CELL_VOLTAGES          = 0x40
    BATTERY_INFO           = 0x30
    SERIAL_NUMBER          = 0x10
    TOTAL_KILOMETER        = 0xb7
    LOCK                   = 0x70
    UNLOCK                 = 0x71
    CRUISE_CONTROL_ON      = 0x7c
    TAIL_LIGHT             = 0x7d
    KERS_KRUISE_TAIL_LIGHT = 0x7b
    SCOOTER_MODE           = 0x72
    BLE_VERSION            = 0x67
    UPTIME                 = 0xb0
    TOTAL_UPTIME           = 0x1a

class Message:
    """class that contains all the necessary methodes to construct the message"""
    def __init__(self):
        self.source = None
        self.destination = None
        self.command = None
        self.arg = None
        self.payload = None

        self._checksum = None
        self._raw_bytes = None

    def set_source(self, source):
        """Sets the messages source"""
        self.source = source
        return self

    def set_destination(self, destination):
        """Sets the messages destination"""
        self.destination = destination
        return self

    def set_command(self, command):
        """Sets the messages command"""
        self.command = command
        return self

    def set_arg(self, arg):
        """Sets the messages argument"""
        self.arg = arg
        return self

    def set_payload(self, payload):
        """Sets the messages payload"""
        self.payload = payload
        return self

    def _calc_checksum(self):
        """Calculates the messages checksum"""
        checksum = 0
        checksum += self.command
        checksum += self.source
        checksum += self.destination
        checksum += self.arg

        # NOTE: python2x and python3x does not have compliant byte literals
        try:
            # python 2.7
            for byte in self.payload:
                byte_val = struct.unpack('>B', byte)[0]
                checksum += byte_val
        except Exception:
            # python 3.x
            for byte_val in self.payload:
                checksum += byte_val

        checksum += len(self.payload)
        checksum ^= 0xffff
        checksum &= 0xffff

        self._checksum = checksum

    def build(self):
        """Builds the message"""
        self._calc_checksum()

        result = bytearray()
        result.extend(struct.pack('<H', HEADER))
        # >>>> these are single byte so we don't have to worry about byte order
        result.append(len(self.payload))
        result.append(self.source)
        result.append(self.destination)
        result.append(self.command)
        result.append(self.arg)

        result.extend(self.payload)
        result.extend(struct.pack('<H', self._checksum))
        self._raw_bytes = bytes(result)
        return self

    @staticmethod
    def parse_from_bytes(message):
        """Constructs the message from the smaller parts"""
        message_length = len(message)
        payload_start = 7

        # pylint: disable=line-too-long
        header, length, source, destination, command, argument = struct.unpack('<HBBBBB', message[:payload_start])
        total_length = payload_start + length + 2

        # print("{} ===== {}".format(total_length, message_length))
        if header != HEADER:
            return ParseStatus.INVALID_HEADER, None
        if total_length != message_length:
            return ParseStatus.DISJOINTED, None

        payload = message[payload_start:(total_length-2)]

        result = Message()                \
            .set_source(source)           \
            .set_destination(destination) \
            .set_command(command)         \
            .set_arg(argument)            \
            .set_payload(payload)         \
            .build()

        if message[:-2] != result._raw_bytes[:-2]:
            return ParseStatus.INVALID_CHECKSUM, None

        return ParseStatus.OK, result


BATTERY_VOLTAGE = Message()                        \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_BMS)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.CELL_VOLTAGES)               \
    .set_payload(b'\x18')                          \
    .build()

BATTERY_INFO = Message()                           \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_BMS)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.BATTERY_INFO)                \
    .set_payload(b'\x18')                          \
    .build()


# 292 Battery serial Number
BATTERY_SERIAL_NUMBER = Message()                  \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_BMS)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.SERIAL_NUMBER)               \
    .set_payload(b'\x22')                          \
    .build()

# 299 Vehicle Serial Number
VEHICLE_SERIAL_NUMBER = Message()                  \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.SERIAL_NUMBER)               \
    .set_payload(b'\x2c')                          \
    .build()

# 304 total km
TOTAL_KILOMETER = Message()                        \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.TOTAL_KILOMETER)             \
    .set_payload(b'\x18')                          \
    .build()

# 701  lock
LOCK_SCOOTER = Message()                           \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.LOCK)                        \
    .set_payload(b'\x01')                          \
    .build()

# 771 unlock
UNLOCK_SCOOTER = Message()                         \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.UNLOCK)                      \
    .set_payload(b'\x01')                          \
    .build()

# 1001  Cruise Control on
CRUISE_ON = Message()                              \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.CRUISE_CONTROL_ON)           \
    .set_payload(b'\x01')                          \
    .build()

# 1093  Cruise Control off
CRUISE_OFF = Message()                             \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.CRUISE_CONTROL_ON)           \
    .set_payload(b'\x00')                          \
    .build()

# 563 tail light on
TAIL_LIGHT_ON = Message()                          \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.TAIL_LIGHT)                  \
    .set_payload(b'\x02\x00')                      \
    .build()

# 1310  tail light off
TAIL_LIGHT_OFF = Message()                         \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.WRITE)                    \
    .set_arg(Argument.TAIL_LIGHT)                  \
    .set_payload(b'\x00\x00')                      \
    .build()

# 317
GET_IF_KERS_CRUISE_TAIL_IS_ON = Message()          \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.KERS_KRUISE_TAIL_LIGHT)      \
    .set_payload(b'\x06')                          \
    .build()

# 320
SCOOTER_MODE = Message()                           \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.SCOOTER_MODE)                \
    .set_payload(b'\x20')                          \
    .build()

# 287    835 a dest valtozott
BLE_VERSION = Message()                            \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.BLE_VERSION)                 \
    .set_payload(b'\x32')                          \
    .build()

# rename it
UPTIME = Message()                                 \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.UPTIME)                      \
    .set_payload(b'\x1c')                          \
    .build()

TOTAL_UPTIME_AND_LEFT_KM = Message()               \
    .set_source(Source.APPLICATION2)               \
    .set_destination(Destination.REQ_TO_ESC)       \
    .set_command(Command.READ)                     \
    .set_arg(Argument.TOTAL_UPTIME)                \
    .set_payload(b'\x46')                          \
    .build()
