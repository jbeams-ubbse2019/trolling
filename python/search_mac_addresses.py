"""
Searches all the Xiaomi and Ninebot kickscooters MAC addresses and calls the script
 which gets the data from the scooters
"""

from __future__ import print_function

import os

from bluepy.btle import Scanner

# Scans for available devices.
SCAN = Scanner()
SEC = 5
COMMAND = "python3 /home/pi/trolling/python/get_data.py "
ADDRESS = ""

DEVS = SCAN.scan(SEC)
for dev in DEVS:
    LOCALNAME = dev.getValueText(9)
    if LOCALNAME and (LOCALNAME.startswith("MIScooter") or LOCALNAME.startswith("NBScooter")):
        os.system(COMMAND + dev.addr + " " + LOCALNAME)
