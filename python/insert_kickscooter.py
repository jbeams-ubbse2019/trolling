"""Insert a new kickscooter into the database"""
import sys

from configparser import ConfigParser

import mysql.connector

PARSE = ConfigParser()
PARSE.read('./database/config.ini')

MY_HOST = PARSE.get("db", "host")
MY_USER_NAME = PARSE.get("db", "user")
MY_PASSWD = PARSE.get("db", "passwd")
MY_DATABASE_NAME = PARSE.get("db", "database")

MY_DB = mysql.connector.connect(
    host = MY_HOST,
    user = MY_USER_NAME,
    passwd = MY_PASSWD,
    database = MY_DATABASE_NAME
)

MY_CURSOR = MY_DB.cursor()
i = 1
ARG_LEN = len(sys.argv)


while i < ARG_LEN:
    MY_CURSOR.callproc("insert_foreign_kickscooter", (sys.argv[i], sys.argv[i+1]) )
    MY_CURSOR.execute('COMMIT')
    i +=  2

MY_CURSOR.execute('COMMIT')
MY_DB.disconnect()
