"""This module is locks a kickscooter, its first argument is the MAC address"""
import sys
import time

from ble_connection import connect
from m365_new import m365message
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = sys.argv[1]
SCOOTER_NAME = sys.argv[2]

if SCOOTER_NAME.startswith("NBScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
    LOCK = ninebot_message.LOCK_SCOOTER
elif SCOOTER_NAME.startswith("MIScooter"):
    SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Xiaomi")
    LOCK = m365message.LOCK_SCOOTER

SCOOTER.connect()

SCOOTER.request(LOCK)

SCOOTER.disconnect()

time.sleep(3)
sys.exit(0)
