"""Example script about the Ninebot protokoll usage"""
import time
import json

from ble_connection import connect
from ninebot import ninebot_message

SCOOTER_MAC_ADDRESS = 'C2:AE:00:25:FC:04'
SCOOTER = connect.Connect(SCOOTER_MAC_ADDRESS, "Ninebot")
SCOOTER.connect()

UPDATE_INTERVAL_S = 5.0
while True:
    START_TIME = time.time()

    # Request all currently supported 'attributes'
    SCOOTER.request(ninebot_message.BATTERY_VOLTAGE)
    SCOOTER.request(ninebot_message.BATTERY_INFO)
    SCOOTER.request(ninebot_message.BATTERY_SERIAL_NUMBER)
    SCOOTER.request(ninebot_message.VEHICLE_SERIAL_NUMBER)
    SCOOTER.request(ninebot_message.TOTAL_KILOMETER)
    SCOOTER.request(ninebot_message.GET_IF_KERS_CRUISE_TAIL_IS_ON)
    SCOOTER.request(ninebot_message.SCOOTER_MODE)
    SCOOTER.request(ninebot_message.BLE_VERSION)
    SCOOTER.request(ninebot_message.TOTAL_UPTIME_AND_LEFT_KM)
    SCOOTER.request(ninebot_message.UPTIME)

    # SCOOTER.request(ninebot_message.LOCK_SCOOTER)
    # SCOOTER.request(ninebot_message.UNLOCK_SCOOTER)

    # SCOOTER.request(ninebot_message.CRUISE_ON)
    # scooter.request(ninebot_message.CRUISE_OFF)

    # SCOOTER.request(ninebot_message.TAIL_LIGHT_ON)
    # SCOOTER.request(ninebot_message.TAIL_LIGHT_OFF)

    # m365py also stores a cached state of received values
    print(str(SCOOTER.cached_state))
    print(json.dumps(SCOOTER.cached_state, indent=4, sort_keys=True))
    # print(json.dumps(SCOOTER.cached_state, indent=4, sort_keys=True))
    # try to consistently run loop every 5 seconds
    ELAPSED_TIME = time.time() - START_TIME
    SLEEP_TIME = max(UPDATE_INTERVAL_S - ELAPSED_TIME, 0)
    time.sleep(SLEEP_TIME)

SCOOTER.disconnect()
