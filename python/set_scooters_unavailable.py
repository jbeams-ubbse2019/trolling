""" Sets all the available scooters into unavailable in the database """
import sys

from configparser import ConfigParser

import mysql.connector

PARSER = ConfigParser()
PARSER.read('/home/pi/trolling/database/config.ini')

MY_HOST = PARSER.get("db", "host")
MY_USERNAME = PARSER.get("db", "user")
MY_PASSWD = PARSER.get("db", "passwd")
MY_DATABASE_NAME = PARSER.get("db", "database")

MY_DB = mysql.connector.connect(
    host = MY_HOST,
    user = MY_USERNAME,
    passwd = MY_PASSWD,
    database = MY_DATABASE_NAME
)

MY_CURSOR = MY_DB.cursor()
i = 1
ARG_LEN = len(sys.argv)
NOT_AVAILABLE_COMMAND = "update kickscooters SET statusId = (SELECT status.id FROM status WHERE name = 'not found') WHERE statusId = (SELECT status.id FROM status WHERE name = 'available');"

MY_CURSOR.execute(NOT_AVAILABLE_COMMAND)

MY_CURSOR.execute('COMMIT')

MY_DB.disconnect()
