FROM node:12.13.0-alpine

WORKDIR /usr/node-example

COPY ./web-server/package*.json ./

RUN npm install

COPY ./web-server/ ./

ENV PORT=3000

EXPOSE 3000

CMD npm start